﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【用户管理】逻辑层
    /// </summary>
    public class Admin_UserService : Repository
    {
        /// <summary>
        ///  角色表数据查询
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RoleModel> GetRoleList()
        {
            var db = this.Base();
            var role = db.IQueryable<Guo_Role>();
            var query = from i in role
                        select new RoleModel()
                        {
                            Id = i.Id,
                            RoleName = i.UserName
                        };

            return query;
        }

        /// <summary>
        /// 用户管理查询
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public LayuiResult GetListByFilter(UserModel filter, PageInfo pageInfo)
        {
            var db = this.Base();
            var user = db.FindList<Guo_User>();
            var roles = db.FindList<Guo_Role>();           
            var query = from u in user
                        join r in roles on new { RoleId = (int)u.RoleId } equals new { RoleId = r.Id }
                        orderby
                          u.CreateOn descending
                        select new
                        {
                            u.ID,
                            u.LoginID,
                            u.UserName,
                            u.CreateOn,
                            u.PassWord,
                            u.Status,
                            RoleId = (int)u.RoleId,
                            RoleName = r.UserName
                        };


            //操作名称
            if (!string.IsNullOrEmpty(filter.UserName))
            {
                string actionName = filter.UserName.ToString();
                query = query.Where(t => t.UserName.Contains(actionName));
            }

            //角色名称
            if (filter.RoleId != 0)
            {
                query = query.Where(t => t.RoleId == filter.RoleId);
            }

            //状态
            if (filter.Status != null)
            {
                bool isActive = Convert.ToBoolean(filter.Status);
                query = query.Where(t => t.Status == isActive);
            }

            var data = query.Skip((pageInfo.page - 1) * pageInfo.limit).Take(pageInfo.limit);//分页
            return new LayuiResult { code = 0, count = user.Count(), data = query.ToList() };
        }

        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateModel(UserModel model)
        {
            var db = this.Base();
            Guo_User article = new Guo_User();
            db.SaveChanges();

            //SqlParameter[] sql = { new SqlParameter("@strSource", model.PassWord), new SqlParameter("@EncPara", model.Id) };
            //var pwd = db.ExecuteByFun<string>("BRPEncrypt", sql);  //调用数据库函数对输入密码加密
            article.LoginID = model.UserName;
            article.UserName = model.RealName;
            article.RoleId = model.RoleId;
            article.Status = model.Status;
            article.PassWord = model.PassWord;
            article.CreateBy = model.CreateBy;
            article.CreateOn = model.CreateOn;

            db.Insert<Guo_User>(article);
            return db.Commit() > 0 ? true : false;

        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<UserModel> ReadModel(int Id)
        {
            var db = this.Base();
            var data = db.IQueryable<Guo_User>();
            var role = db.IQueryable<Guo_Role>();
            var query = from i in data
                        join t in role
                        on new { RoleId = (int)i.RoleId, i.ID }
                        equals new { RoleId = t.Id, ID = Id }
                        select new UserModel()
                        {
                            Id = i.ID,
                            UserName = i.LoginID,
                            RealName = i.UserName,
                            Status = i.Status,
                            RoleName = t.UserName,
                            CreateOn = i.CreateOn,
                            UpdateOn = i.UpdateOn,
                            CreateBy = i.CreateBy,
                            UpdateBy = i.UpdateBy
                        };

            return query.ToList();
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateList(UserModel model)
        {
            var db = this.Base();
            var res = db.IQueryable<Guo_User>(t => t.ID == model.Id).FirstOrDefault();
            res.LoginID = model.UserName;
            res.UserName = model.RealName;
            res.RoleId = model.RoleId;
            res.Status = model.Status;
            res.UpdateBy = model.UpdateBy;
            res.UpdateOn = model.UpdateOn;

            db.Update(res);
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteModel(int Id)
        {
            var db = this.Base();
            try
            {
                var entity = db.FindEntity<Guo_User>(t => t.ID == Id);
                if (entity == null)
                {
                    return false;
                }
                db.Delete<Guo_User>(t => t.ID == entity.ID);
                return db.Commit() > 0 ? true : false;
            }
            catch (Exception)
            {
                // db.Rollback();
                throw new Exception("删除失败！");
            }
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool InitPwd(UserModel model)
        {
            var db = this.Base();
            var user = db.FindEntity<Guo_User>(t => t.ID == model.Id);
            user.PassWord = model.PassWord;
            user.UpdateOn = DateTime.Now;
            user.UpdateBy = Current.GetEntity().OperatorId;

            db.Update<Guo_User>(user);
            return db.Commit() > 0 ? true : false;
        }
    }
}
