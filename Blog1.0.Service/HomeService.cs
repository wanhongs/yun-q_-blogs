﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using Until;

namespace Blog1._0.Service
{
    public class HomeService : Repository
    {
        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public string GetListJson(int PageNum, int PageSize)
        {
            var db = this.Base();
            //var queryForm = queryJson.ToJObject();
            //string ArticleTitle = queryForm["ArticleTitle"].ToString().Replace(';', ',').IsEmpty() ? "" : queryForm["ArticleTitle"].ToString().Replace(';', ',');
            int CatalogID = 0;
            string ArticleTitle = "";
            int UserID = 0;
            string Content = "";
            string OrderFields = "";
            string RecordsTotal = "";
            List<DbParameter> sqlParameter = new List<DbParameter>();
            sqlParameter.Add(new SqlParameter("@CatalogID", SqlDbType.Int));
            sqlParameter.Add(new SqlParameter("@ArticleTitle", SqlDbType.VarChar));
            sqlParameter.Add(new SqlParameter("@UserID", SqlDbType.Int));
            sqlParameter.Add(new SqlParameter("@Content", SqlDbType.VarChar));
            sqlParameter.Add(new SqlParameter("@PageNum", SqlDbType.Int));
            sqlParameter.Add(new SqlParameter("@PageSize", SqlDbType.Int));
            sqlParameter.Add(new SqlParameter("@OrderFields", SqlDbType.VarChar));
            sqlParameter.Add(new SqlParameter("@RecordsTotal", SqlDbType.VarChar));
            sqlParameter[0].Value = CatalogID;
            sqlParameter[1].Value = ArticleTitle;
            sqlParameter[2].Value = UserID;
            sqlParameter[3].Value = Content;
            sqlParameter[4].Value = PageNum;
            sqlParameter[5].Value = PageSize;
            sqlParameter[6].Value = OrderFields;
            sqlParameter[7].Value = RecordsTotal;
            var query = db.ExecuteByProcDataSet("UP_ArticleListGet", sqlParameter.ToArray()).Tables[0];
            return query.ToJson();
        }

        /// <summary>
        /// 获取标签
        /// </summary>
        /// <returns></returns>
        public string GetListLabelInfo()
        {
            var list = this.Base().IQueryable<Guo_Catalog>();
            return list.ToJson();
        }

        /// <summary>
        /// 获取标题
        /// </summary>
        /// <returns></returns>
        public string GetTitle(int ArticleId)
        {
            var list = this.Base().FindEntity<Guo_Acticle>(t => t.ArticleID == ArticleId);
            var query = new {
                ArticleID = list.ArticleID,
                ArticleTitle = list.ArticleTitle
            };
            return query.ToJson();
        }

        /// <summary>
        /// 指定文章
        /// </summary>
        /// <param name="ArticleId"></param>
        /// <returns></returns>
        public string GetArticleList(int ArticleId)
        {
            var db = this.Base();
            var GuoAct = db.FindEntity<Guo_Acticle>(t => t.ArticleID == ArticleId);
            var GuoUser = db.IQueryable<Guo_User>();
            var GuoCar = db.IQueryable<Guo_Catalog>();
            if (GuoAct == null)
            {
                throw new Exception("无法查看该博客！");
            }
            var data = new
            {
                ArticleID = GuoAct.ArticleID,
                ArticleTitle = GuoAct.ArticleTitle,
                Content = GuoAct.Content,
                CRT_Time = GuoAct.CRT_Time,
                UserName = GuoUser.Where(t => t.ID == GuoAct.UserID).Count() > 0 ? GuoUser.Where(t => t.ID == GuoAct.UserID).FirstOrDefault().UserName : "佚名",
                ViewTimes = GuoAct.ViewTimes,
                Year = GuoAct.ViewTimes                
            };
            return data.ToJson();
        }

        /// <summary>
        /// 插入 文章点击率
        /// </summary>
        /// <param name="ArticleId"></param>
        /// <returns></returns>
        public Result GetInsertView(int ArticleId)
        {
            var db = this.Base();
            var GuoAct = db.FindEntity<Guo_Acticle>(t => t.ArticleID == ArticleId);
            GuoAct.ViewTimes += 1;
            db.Update(GuoAct);
            if (db.Commit() > 0)
            {
                return new Result { type = true };
            }
            else
            {
                return new Result { type = false,msg = "出错" };
            } 

        }
    }
}
