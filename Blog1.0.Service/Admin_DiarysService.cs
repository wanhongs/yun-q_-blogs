﻿using Blog1._0.Data;
using Blog1._0.Model;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【日记】逻辑层
    /// </summary>
    public class Admin_DiarysService : Repository
    {
        /// <summary>
        /// 返回日期中 年份
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DiarysModel> Getyear()
        {
            var db = this.Base();
            var diay = db.IQueryable<Guo_Diays>();
            var query = from guo_qquser in diay
                        group guo_qquser by new
                        {
                            Column1 = (int)SqlFunctions.DatePart("year", guo_qquser.CreateOn)
                        } into g
                        orderby
                          g.Key.Column1 descending
                        select new DiarysModel()
                        {
                            Years = g.Key.Column1
                        };
            return query.ToList();
        }

        /// <summary>
        /// 查询日记数据
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DiarysModel> GetAll()
        {
            var db = this.Base();
            var diay = db.IQueryable<Guo_Diays>();
            var query = from q in diay
                        orderby q.CreateOn descending
                        select new DiarysModel()
                        {
                            Id = q.Id,
                            Content = q.Content,
                            CreateOn = q.CreateOn
                        };
            return query.ToList();
        }
    }
}
