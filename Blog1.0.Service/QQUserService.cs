﻿using Blog1._0.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Until;
using Blog1._0.Model;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【QQ授权】逻辑层
    /// </summary>
    public class QQUserService : Repository
    {
        /// <summary>
        /// 查询QQ用户信息
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        public IQueryable<Guo_QQUser> GetQQUserInfo(string openid)
        {
            var db = this.Base();
            var query = db.IQueryable<Guo_QQUser>(t => t.OpenId == openid);
            return query != null ? query : null;
        }


    }
}
