﻿using System;
using System.Data.Entity.Infrastructure;

namespace Blog1._0.Data
{
    public interface IDbContext : IDisposable, IObjectContextAdapter
    {
    }
}
