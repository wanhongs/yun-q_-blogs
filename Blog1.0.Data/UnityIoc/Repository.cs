﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Until;

namespace Blog1._0.Data
{
    public class Repository
    {
        /// <summary>
        /// 连接默认库
        /// </summary>
        /// <returns></returns>
        public IDatabase Base()
        {
            return Base("Guo_Blogs");
        }

        /// <summary>
        /// 连接数据库
        /// </summary>
        /// <param name="connString">数据库名称</param>
        /// <returns></returns>
        public IDatabase Base(string connString)
        {
            var str = ConfigurationManager.ConnectionStrings[connString].ConnectionString;

            return UnityIocHelper.DBInstance.GetService<IDatabase>(new ParameterOverride(
              "connString", str), new ParameterOverride(
              "DbType", "SqlServer"));
        }
    }
}
