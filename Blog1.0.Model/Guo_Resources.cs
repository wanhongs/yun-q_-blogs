﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blog1._0.Model
{
    /// <summary>
    /// 资源 分类
    /// </summary>
    public partial class Guo_Resources
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        public int UserID { get; set; }        
        /// <summary>
        /// 排序
        /// </summary>
        public Nullable<int> OrderNo { get; set; }      
        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<System.DateTime> CRT_Time { get; set; }
    }
}
