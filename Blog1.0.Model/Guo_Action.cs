﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blog1._0.Model
{
    /// <summary>
    /// 菜单权限按钮 类
    /// </summary>
    public partial class Guo_Action
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]//代表ID自增
        public int ID { get; set; }
        /// <summary>
        /// 操作编码
        /// </summary>
        public string ActionCode { get; set; }
        /// <summary>
        /// 操作名称
        /// </summary>
        public string ActionName { get; set; }
        /// <summary>
        /// 显示位置（1：表单内，2：表单右上）
        /// </summary>
        public Nullable<int> Position { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 方法名称
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        public Nullable<int> OrderBy { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Nullable<bool> Status { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<System.DateTime> CreateOn { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public Nullable<System.DateTime> UpdateOn { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        public Nullable<int> CreateBy { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        public Nullable<int> UpdateBy { get; set; }
        /// <summary>
        /// 样式名称
        /// </summary>
        public string ClassName { get; set; }

    }
}
