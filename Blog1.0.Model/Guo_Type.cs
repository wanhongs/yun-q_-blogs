﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    /// <summary>
    /// 文章来源属性 类
    /// </summary>
    public partial class Guo_Type
    {        
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TypeID { get; set; }
        /// <summary>
        /// 文章来源属性 名
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        public Nullable<int> UserID { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public Nullable<bool> IsActive { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<System.DateTime> CRT_Time { get; set; }       
    }
}
