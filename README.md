﻿# YunQ_Blogs

#### 项目介绍<br><br>  
  
为个人业余作品（个人博客）。前台借鉴燕十三博客前端模板，后台基于ASP.NET MVC通用角色权限管理系统搭建，大体功能已完成。用户可以登录QQ，进行查看文章详情、评论、留言、点赞等。<br><br>


>  **项目演示地址：http://www.guoqingyun.top/** 

>  **如果您在使用与学习过程中，遇到问题无法解决，请您在仔细思考无果后，也没解决问题，届时欢迎叨扰。授人予鱼不如授人与渔，请您一定首先自己思考。感谢**

>  **如果对你有帮助的话请右上角点个star，非常感谢！** <br><br>

#### 使用说明<br><br>

1. 数据库文件在Sql文件夹中。博主使用版本为SQLServer2008R2，高版本直接执行sql文件即可。相同数据库版本附加mdf文件即可。
2. 直接使用的话，本地调试是没问题的，您要发布到广域网上，只需您申请域名、购买云服务器。就可进行发布使用。
3. 后台管理账号:admin 密码：123456。
4. 要使用QQ登录功能，需要您申请QQ互联开发者，以及应用注册。详情戳我博客。<br><br>


#### 项目说明<br><br>

1. 前台使用Layui开源框架，组件清新、美观。感兴趣的小伙儿也可移步一观。
2. 接入QQ互联，一键登录，降低注册门槛。
3. LayUI流加载文章列表、评论。
4. 网站设置，新增首页背景图更换。
5. 后台管理新增文章统计、用户统计以及今日留言等。<br><br>


#### 使用的相关技术<br><br>

1. ASP.NET MVC5
2. SQLSERVER2008R2（数据库）
3. LAYUI（前端框架）
4. EntityFramework CodeFirst（ORM框架） 
5. Unity（IOC容器）
6. Log4Net（日志）<br><br>



#### 项目截图<br><br>
![Image text](https://gitee.com/guo_zi_shan/yun-q_-blogs/raw/master/Images/1.png)
![Image text](https://gitee.com/guo_zi_shan/yun-q_-blogs/raw/master/Images/2.png)
![Image text](https://gitee.com/guo_zi_shan/yun-q_-blogs/raw/master/Images/3.png)
![Image text](https://gitee.com/guo_zi_shan/yun-q_-blogs/raw/master/Images/4.png)