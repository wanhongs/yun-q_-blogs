﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Until
{
    /// <summary>
    /// 日记返回类
    /// </summary>
    public class DiarysModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 日记内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public  DateTime CreateOn { get; set; }
        /// <summary>
        /// 年份
        /// </summary>
        public int Years { get; set; }
    }
}
