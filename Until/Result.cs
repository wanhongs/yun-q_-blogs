﻿namespace Until
{
    public class Result
    {
        /// <summary>
        /// 操作结果类型
        /// </summary>
        public bool type { get; set; }
        /// <summary>
        /// 操作反馈消息
        /// </summary>
        public string msg { get; set; }
        /// <summary>
        /// 具体数据
        /// </summary>
        public object data { get; set; }
    }
}
