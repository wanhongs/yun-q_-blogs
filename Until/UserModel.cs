﻿using System;

namespace Until
{
    public class UserModel
    {
        public int Id { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 真实名称
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string PassWord { get; set; }
        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// 性别（0：女，1：男）
        /// </summary>
        public int? Gender { get; set; }
        /// <summary>
        /// 手机
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadShot { get; set; }
        public bool? Status { get; set; }
        public string RoleName { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateOn { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateOn { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        public int? CreateBy { get; set; }
        /// <summary>
        /// 修改者
        /// </summary>
        public int? UpdateBy { get; set; }

    }
}
