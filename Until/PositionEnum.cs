﻿namespace Until
{
    /// <summary>
    /// 按钮 显示位置 枚举类
    /// </summary>
    public enum PositionEnum
    {
        /// <summary>
        /// 表内
        /// </summary>
        FormInside = 0,
        /// <summary>
        /// 表外
        /// </summary>
        FormRightTop = 1
    }
}
