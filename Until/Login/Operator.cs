﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Until
{
    public class Operator
    {
        /// <summary>
        /// 操作员Id
        /// </summary>
        public int OperatorId { get; set; }
        /// <summary>
        /// 角色Id
        /// </summary>
        public int? RoleId { get; set; }
        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 用户编号(登录账号)
        /// </summary>
        public string LoginID { get; set; }
        //// <summary>
        /// 用户头像
        /// </summary> 
        public string HeadShot { get; set; }
        /// <summary>
        /// 登录IP地址
        /// </summary>
        public string IPAddress { get; set; }
        /// <summary>
        /// 登录IP地址所在地址
        /// </summary>
        public string IPAddressName { get; set; }
        /// <summary>
        /// 登陆时间
        /// </summary>
        public string LoginTime { get; set; }
        /// <summary>
        /// 是否系统账户；拥有所有权限
        /// </summary>
        public bool IsSystem { get; set; }
    }
}
