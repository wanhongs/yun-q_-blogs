﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Until
{
    public enum Category
    {
        /// <summary>
        /// 登录
        /// </summary>
        Login = 1,
        /// <summary>
        /// 访问
        /// </summary>
        Info = 2,
        /// <summary>
        /// 操作
        /// </summary>
        Warn = 3,
        /// <summary>
        /// 异常
        /// </summary>
        Debug = 4
    }
}
