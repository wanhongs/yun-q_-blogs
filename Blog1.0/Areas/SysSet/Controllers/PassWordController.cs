﻿using Blog1._0.Service;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.SysSet.Controllers
{
    /// <summary>
    /// 【修改密码】控制器
    /// </summary>
    public class PassWordController : MvcControllerBase
    {
        Admin_PassWordService service = new Admin_PassWordService();
        /// <summary>
        /// 修改密码 视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Index(int? Id)
        {
            #region 查询按钮权限
            ActionService actionService = new ActionService();
            var _menuId = Id == null ? 0 : Id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (Id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            } 
            #endregion
            ViewBag.UserName = Current.GetEntity().LoginID;
            return View();
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(PassWordModel model)
        {
            ViewBag.UserName = model.UserName;
            if (service.CheckPwd(Md5s.md5(model.OldPassword, 32)) == false)
            {
                ViewBag.Msg = "原始密码输入错误";
                return View();
            }

            if (model.Password == model.OldPassword)
            {
                ViewBag.Msg = "密码不能与原密码一致";
                return View();
            }
            else
            {
                if (service.ModifyPwd(model))
                {
                    ViewBag.Msg = "密码修改成功";
                }
                else
                {
                    ViewBag.Msg = "密码修改失败";

                }
            }
            return View();
        }
    }
}