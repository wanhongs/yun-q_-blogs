﻿using Blog1._0.Service;
using System.Linq;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.Blog.Controllers
{
    /// <summary>
    /// 【用户管理】控制器
    /// </summary>
    public class QQUserController : MvcControllerBase
    {
        Admin_QQUserService service = new Admin_QQUserService();
        /// <summary>
        /// 用户管理首页视图（QQ登录用户）
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Index(int? Id)
        {
            #region 查询按钮权限
            ActionService actionService = new ActionService();
            var _menuId = Id == null ? 0 : Id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (Id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            }
            #endregion
            return View();
        }

        /// <summary>
        /// 详情视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Detail(int Id)
        {
            var model = service.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 修改视图
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            var model = service.ReadModel(id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult List(QQUserModel filter, PageInfo pageInfo)
        {
            var result = service.GetListByFilter(filter, pageInfo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(QQUserModel model)
        {
            var result = service.UpdateList(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            var result = service.DeleteModel(Id) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

    }
}