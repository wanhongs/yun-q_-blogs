﻿using Blog1._0.Model;
using Blog1._0.Service;
using System;
using System.Linq;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.Blog.Controllers
{
    /// <summary>
    /// 【文章管理】控制器
    /// </summary>
    public class ArticleController : MvcControllerBase
    {
        Admin_ArticleService admin = new Admin_ArticleService();
        /// <summary>
        /// 【文章管理】 视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int? id)
        {
            ViewBag.CatalogID = ArticleClassList;
            ViewBag.TypeID = ArticleTypeList;        
                
            #region 查询对应菜单按钮权限
            ActionService actionService = new ActionService();
            var _menuId = id == null ? 0 : id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            } 
            #endregion
            return View();
        }

        /// <summary>
        /// 【编辑】 视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(int Id)
        {
            Admin_ArticleService articleService = new Admin_ArticleService();
            ViewBag.CatalogID = ArticleClassList;
            ViewBag.TypeID = ArticleTypeList;
            //数据加载
            var model = articleService.GetDetail(Id).FirstOrDefault();            
            return View(model);
        }

        /// <summary>
        /// 【文章详情】 视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Detail(int Id)
        {
            Admin_ArticleService articleService = new Admin_ArticleService();
            var model = articleService.GetDetail(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 【新增文章】 视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            ViewBag.CatalogID = ArticleClassList;
            ViewBag.TypeID = ArticleTypeList;
            ViewBag.MaxFileUpload = Configs.GetValue("MaxFileUpload");
            ViewBag.UploadFileType = Configs.GetValue("UploadFileType");
            return View();
        }

        /// <summary>
        /// 文章标签 下拉列表
        /// </summary>
        public SelectList ArticleClassList
        {
            get {
                return new SelectList(admin.GetTypeList(), "Id", "Name");
            }
        }

        /// <summary>
        /// 文章分类 下拉列表
        /// </summary>
        public SelectList ArticleTypeList
        {
            get
            {
                return new SelectList(admin.GetArticleTypeList(), "Id", "Name");
            }
        }

        /// <summary>
        /// 查询文章
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetListJson(Guo_Acticle filter, PageInfo pageInfo)
        {
            var result = admin.GetListByFilter(filter, pageInfo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 修改文章
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(ArticleModel model)
        {
            Admin_ArticleService articleService = new Admin_ArticleService();
            model.Up_Time = DateTime.Now;
            var result = articleService.UpdateList(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 删除指定文章
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            Admin_ArticleService articleService = new Admin_ArticleService();
            var result = articleService.DeleteModel(Id) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 新增文章
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(ArticleModel model)
        {
            //点击率与评论数归于0
            model.ViewTimes = 0;
            model.Replies = 0;
            model.Up_Time = DateTime.Now;
            Admin_ArticleService articleService = new Admin_ArticleService();
            var result = articleService.CreateModel(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }
    }
}