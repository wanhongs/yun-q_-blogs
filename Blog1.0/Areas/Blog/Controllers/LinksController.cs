﻿using Blog1._0.Service;
using System;
using System.Linq;
using System.Web.Mvc;
using Until;
using Blog1._0.Model;

namespace Blog1._0.Areas.Blog.Controllers
{
    /// <summary>
    /// 【友情链接】控制器
    /// </summary>
    public class LinksController : MvcControllerBase
    {
        Admin_LinksService linksService = new Admin_LinksService();
        /// <summary>
        /// 友情链接 视图
        /// </summary>
        /// <returns></returns>        
        public ActionResult Index(int? Id)
        {
            #region 查询按钮权限
            ActionService actionService = new ActionService();
            var _menuId = Id == null ? 0 : Id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (Id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            }
            #endregion
            return View();
        }

        /// <summary>
        /// 添加友链
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 数据查询
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetLinkList(Guo_Links filter, PageInfo pageInfo)
        {
            var result = linksService.GetListByFilter(filter, pageInfo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 添加友链-方法
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(LinksModel model)
        {
            model.CreateOn = DateTime.Now;
            var result = linksService.CreateModel(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 详情视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Detail(int Id)
        {
            var model = linksService.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 编辑视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(int Id)
        {
            var model = linksService.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(LinksModel model)
        {
            var result = linksService.UpdateList(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            var result = linksService.DeleteModel(Id) ? SuccessTip() : ErrorTip();
            return Json(result);
        }
    }
}