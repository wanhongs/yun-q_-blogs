﻿using Blog1._0.Service;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.Admin.Controllers
{
    /// <summary>
    /// 【登录】控制器
    /// </summary>
    [LoginAuthorize(LoginMode.Ignore)]
    public class LoginController : MvcControllerBase
    {       
        /// <summary>
        /// 登录视图（新版）
        /// </summary>
        /// <returns></returns>
        public ActionResult Test()
        {
            ViewBag.Title = Configs.GetValue("SiteName");//获取网站信息            
            ViewBag.CopyRight = Configs.GetValue("CopyRight");//获取版权信息            
            ViewBag.SiteDomain = Configs.GetValue("SiteDomain");//获取本站域名
            Session["Operator"] = null;//清除session
            return View();
        }      

        /// <summary>
        /// 验证码
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetAuthCode()
        {
            return File(new VerifyCode().GetVerifyCode(), @"image/Gif");
        }

        /// <summary>
        /// 登录方法
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CheckLogin(string username, string password, string code)
        {
            if (Session["nfine_session_verifycode"].IsEmpty() || Md5s.md5(code.ToLower(), 16) != Session["nfine_session_verifycode"].ToString())
            {
                return Error("验证码错误");
            }
            LoginService userEntity = new LoginService();
            var res = userEntity.UserLogin(username, Md5s.md5(password,32));
            if (res.type)
            {
                var data = res.data as Operator;
                if (data.LoginID == "admin")
                {
                    data.IsSystem = true;
                }
                else
                {
                    data.IsSystem = false;
                }
                Session["Operator"] = data;

                WriteLog(res, Category.Login);//写入日志
                res.data = null;
                return Success(res.msg);
            }
            else
            {
                return Error(res.msg);
            }
        }

    }
}