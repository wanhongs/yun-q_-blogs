﻿using Blog1._0.Service;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.Admin.Controllers
{
    /// <summary>
    /// 【后台管理首页】控制器
    /// </summary>
    public class AdminHomeController : MvcControllerBase
    {
        /// <summary>
        /// 后台管理首页 视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var user = Session["Operator"] as Operator;
            ViewBag.Title = Configs.GetValue("SiteName");//获取网站信息            
            ViewBag.CopyRight = Configs.GetValue("CopyRight");//获取版权信息       
            return View(user);
        }

        /// <summary>
        /// 控制面板
        /// </summary>
        /// <returns></returns>
        public ActionResult Main()
        {
            ArticleService article = new ArticleService();
            Admin_QQUserService quser = new Admin_QQUserService();
            Admin_ArticleService articles = new Admin_ArticleService();
            Admin_FeedbackService feedback = new Admin_FeedbackService();
            ViewBag.articles = article.GetByQQUserNum();
            ViewBag.qqusers = quser.GetByQQUserNums();
            ViewBag.artilenums = articles.GetArticleNum();
            ViewBag.feedback = feedback.GetFeedbackNum();
            return View();
        }

        /// <summary>
        /// 刷新session
        /// </summary>
        /// <returns></returns>
        public ActionResult ReloadSession()
        {
            var entity = Current.GetEntity();
            if (entity != null)
            {
                var user = Session["Operator"];
                Session["Operator"] = null;
                Session["Operator"] = user;
                return Success();
            }
            else
            {
                return Error();
            }
        }

        /// <summary>
        /// 获取菜单列表
        /// </summary>
        /// <param name="isIndex"></param>
        /// <returns></returns>
        public JsonResult GetMenuList(bool isIndex = false)
        {
            AdminService admin = new AdminService();
            var user = Session["Operator"] as Operator;
            object result = admin.GetMenusList(isIndex, user.RoleId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}