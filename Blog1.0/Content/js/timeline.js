﻿layui.use(["jquery", "flow"], function () {//用layui内置jq
    var n = layui.jquery;//n等同于$
    n(".yearToggle").click(function () {
        n(this).parent("h2").siblings(".timeline-month").slideToggle("slow");
        n(this).siblings("i").toggleClass("fa-caret-down fa-caret-right")
    })
})