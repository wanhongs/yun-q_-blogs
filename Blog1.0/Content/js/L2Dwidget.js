﻿L2Dwidget.init({
    "model": {
        jsonPath: "https://unpkg.com/live2d-widget-model-koharu@1.0.5/assets/koharu.model.json",
        "scale": 1//模型与canvas的缩放
    },
    "display": {
        "position": "right",//位置
        "width": 80,//宽度
        "height": 160,//高度
        "hOffset": 40,//canvas水平偏移
        "vOffset": -10//canvas垂直偏移
    },
    "mobile": {
        "show": true,//是否在移动设备上显示
        "scale": 0.5//移动设备上的缩放
    },
    "react": {
        "opacityDefault": 0.8,//透明度
        "opacityOnHover": 0.2//在canvas周围显示边界
    },
    "dialog": {
        "enable": true,//显示人物对话框,只是部分模型才有此功能
        "script": {
            //每20s，显示一言（调用一言Api返回的句子）
            'every idle 20s': '$hitokoto$',
            //触摸到class='star'对象
            'hover .star': '星星在天上而你在我心里 (*/ω＼*)',
            //触摸到身体
            'tap body': '害羞⁄(⁄ ⁄•⁄ω⁄•⁄ ⁄)⁄',
            //触摸到头部
            'tap face': '~~'
        }
    }
});