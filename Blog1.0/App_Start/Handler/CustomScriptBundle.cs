﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Blog1._0
{
    /// <summary>
    /// 自定义混淆加密js 类
    /// </summary>
    public class CustomScriptBundle : Bundle
    {
        public CustomScriptBundle(string virtualPath) : this(virtualPath, null)
        {

        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <param name="cdnPath"></param>
        public CustomScriptBundle(string virtualPath, string cdnPath) : base(virtualPath, cdnPath, new IBundleTransform[]
        {
            //混淆加密js的类
           new CustomJsMinify()
        })
        {
            base.ConcatenationToken = ";";
        }
    }
}