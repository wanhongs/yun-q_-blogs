﻿using System.Web;
using System.Web.Optimization;

namespace Blog1._0
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //JS混淆加密
            //bundles.Add(new CustomScriptBundle("~/bundles/layout").Include(
            //            "~/Scripts/layout.js"
            //            ));

            //bundles(new stylebundle("虚拟路径名，可以随便起"),Include())
            //LayUI
            bundles.Add(new StyleBundle("~/Content/layui/css/layui.css").Include(
                    "~/Content/layui/css/layui.css"));
            bundles.Add(new ScriptBundle("~/Content/layui/layui.js").Include(
                      "~/Content/layui/layui.js"));

            //Home首页
            bundles.Add(new StyleBundle("~/Content/css/index_style.css").Include(
                      "~/Content/css/index_style.css"));
            bundles.Add(new ScriptBundle("~/Content/js/index.js").Include(
                      "~/Content/js/index.js"));

            //Article博客
            bundles.Add(new StyleBundle("~/Content/css/blog.css").Include(
                     "~/Content/css/blog.css"));
            bundles.Add(new ScriptBundle("~/Content/js/yss/article.js").Include(
                      "~/Content/js/yss/article.js"));
            bundles.Add(new StyleBundle("~/Areas/Admin/Content/css/default.css").Include(
                     "~/Areas/Admin/Content/css/default.css"));
            bundles.Add(new ScriptBundle("~/Content/js/pagecomment.js").Include(
                     "~/Content/js/pagecomment.js"));
            bundles.Add(new ScriptBundle("~/Content/js/plugins/qrcode.min.js").Include(
                     "~/Content/js/plugins/qrcode.min.js"));
            bundles.Add(new ScriptBundle("~/Areas/Admin/Content/js/highlight.pack.js").Include(
                     "~/Areas/Admin/Content/js/highlight.pack.js"));

            //前台通用样式
            bundles.Add(new StyleBundle("~/Content/css/master.css").Include(
                      "~/Content/css/master.css"));
            bundles.Add(new StyleBundle("~/Content/css/gloable.css").Include(
                      "~/Content/css/gloable.css"));
            bundles.Add(new StyleBundle("~/Content/css/nprogress.css").Include(
                      "~/Content/css/nprogress.css"));
            bundles.Add(new ScriptBundle("~/Content/js/yss/gloable.js").Include(
                      "~/Content/js/yss/gloable.js"));
            bundles.Add(new ScriptBundle("~/Content/js/plugins/nprogress.js").Include(
                      "~/Content/js/plugins/nprogress.js"));

            //Diarys
            bundles.Add(new StyleBundle("~/Content/css/timeline.css").Include(
                     "~/Content/css/timeline.css"));
            bundles.Add(new ScriptBundle("~/Content/js/timeline.js").Include(
                      "~/Content/js/timeline.js"));

            //Feedback
            bundles.Add(new StyleBundle("~/Content/css/message.css").Include(
                     "~/Content/css/message.css"));
            bundles.Add(new ScriptBundle("~/Content/js/pagemessage.js").Include(
                      "~/Content/js/pagemessage.js"));

            //Links
            bundles.Add(new StyleBundle("~/Content/css/about.css").Include(
                     "~/Content/css/about.css"));
            bundles.Add(new ScriptBundle("~/Content/js/plugins/blogbenoitboucart.min.js").Include(
                      "~/Content/js/plugins/blogbenoitboucart.min.js"));

            //Archives
            bundles.Add(new StyleBundle("~/Content/css/archives.css").Include(
                     "~/Content/css/archives.css"));
            

            //启用压缩js和css
            //BundleTable.EnableOptimizations = true;
        }
    }
}
