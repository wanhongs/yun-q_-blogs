﻿using Blog1._0.Service;
using System.Web.Mvc;

namespace Blog1._0.Controllers
{
    /// <summary>
    /// 【日记】控制器
    /// </summary>
    public class DiarysController : Controller
    {
        /// <summary>
        /// 日记视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            Admin_DiarysService service = new Admin_DiarysService();
            ViewData["Year"] = service.Getyear();//返回年份            
            ViewData["DiarysList"] = service.GetAll();//数据
            return View();
        }
    }
}