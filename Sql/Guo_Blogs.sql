USE [master]
GO
/****** Object:  Database [Guo_Blogs]    Script Date: 10/16/2020 11:40:46 ******/
CREATE DATABASE [Guo_Blogs] ON  PRIMARY 
( NAME = N'Guo_Blogs', FILENAME = N'E:\SQL SERVER\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Guo_Blogs.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Guo_Blogs_log', FILENAME = N'E:\SQL SERVER\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Guo_Blogs_1.ldf' , SIZE = 4224KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Guo_Blogs] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Guo_Blogs].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Guo_Blogs] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Guo_Blogs] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Guo_Blogs] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Guo_Blogs] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Guo_Blogs] SET ARITHABORT OFF
GO
ALTER DATABASE [Guo_Blogs] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [Guo_Blogs] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Guo_Blogs] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Guo_Blogs] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Guo_Blogs] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Guo_Blogs] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Guo_Blogs] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Guo_Blogs] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Guo_Blogs] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Guo_Blogs] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Guo_Blogs] SET  DISABLE_BROKER
GO
ALTER DATABASE [Guo_Blogs] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Guo_Blogs] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Guo_Blogs] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Guo_Blogs] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Guo_Blogs] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Guo_Blogs] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Guo_Blogs] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Guo_Blogs] SET  READ_WRITE
GO
ALTER DATABASE [Guo_Blogs] SET RECOVERY FULL
GO
ALTER DATABASE [Guo_Blogs] SET  MULTI_USER
GO
ALTER DATABASE [Guo_Blogs] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Guo_Blogs] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'Guo_Blogs', N'ON'
GO
USE [Guo_Blogs]
GO
/****** Object:  StoredProcedure [dbo].[UP_GetReplysByArtID]    Script Date: 10/16/2020 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--ID,ArticleID,ReplyerID,ReceiverIDs,Content,LVL,RplTime	-- 暂时不用嵌套回复

--UP_GetReplysByArtID 1,'','','','','','',''
Create   PROCEDURE [dbo].[UP_GetReplysByArtID](
	@ArticleID	INT,
	@ReplyerID INT,	
	@Content NVARCHAR(512),
	@LVL INT,
	@PageNum INT,
	@PageSize INT,
	@OrderFields NVARCHAR(50),
	@RecordsTotal INT OUTPUT
)
AS

DECLARE @SQL NVARCHAR(3000), @sqlWhere NVARCHAR(3000), @tempRecordsTotal INT,@topAddOne INT
--PRINT @ArticleID
IF @PageNum < 1 
	SET @PageNum = 1
SET @sqlWhere = N' WHERE rpl.IsActive = 1 AND ArticleID = ' + CAST(@ArticleID AS NVARCHAR(18)) 
IF @ReplyerID > 0
	SET @sqlWhere = @sqlWhere + N' AND rpl.ReplyerID = ''' + CAST(@ReplyerID AS NVARCHAR(18)) + N''''
IF @Content <> ''
	SET @sqlWhere = @sqlWhere + N' AND rpl.Content LIKE ''%' + CAST(@Content AS NVARCHAR(18)) + N'%'''
SET @tempRecordsTotal = @PageNum * @PageSize
SET @topAddOne = (@PageNum - 1) * @PageSize
SET @SQL = N'
DECLARE @PageIndex INT 
SET @PageIndex = ' + CAST(@PageNum AS NVARCHAR(18)) + ' 
SET NOCOUNT ON
SELECT rpl.ID as ReplyID,rpl.ArticleID as ArticleID,rpl.ReplyerID,rpl.ReceiverIDs,rpl.Content,rpl.LVL,rpl.RplTime, u.UserName as ReplyerName 
INTO #temptable  
FROM T_CatalogReply rpl LEFT JOIN  
_BRP_BI..BI_E_Info u ON rpl.ReplyerID = u.BI_ID  ' + @sqlWhere + ' ORDER BY ReplyID DESC
SELECT @rt = COUNT(1) FROM #temptable 
IF @rt < ' + CAST(@tempRecordsTotal AS NVARCHAR(18)) + ' 
SET @PageIndex = CONVERT(INT,ROUND(@rt / (' + CAST(@PageSize AS NVARCHAR(18)) + ' + 0.0),0)) 
--SET @topAddOne = (@PageIndex - 1) * ' + CAST(@PageSize AS NVARCHAR(18)) + ' 
SELECT * FROM (SELECT TOP ' + CAST(@PageSize AS NVARCHAR(18)) + ' * FROM #temptable  
WHERE ReplyID < 
	( 
		SELECT ISNULL(MIN(ReplyID),9999999) 
		FROM ( 
			SELECT TOP ' + CAST(@topAddOne AS NVARCHAR(18)) + ' ReplyID     
			FROM #temptable
			) AS T
	) ORDER BY ReplyID DESC)as T1 ORDER BY ReplyID DESC -- 用ID替换时间，提高查询效率
SELECT ISNULL(@rt,0) AS RecordsTotal,ISNULL(@PageIndex,0) AS PaseIndex
DROP TABLE #temptable
'
--PRINT @SQL
--EXEC(@SQL)
EXEC SP_EXECUTESQL @SQL,N'@rt INT OUTPUT',@RecordsTotal OUTPUT

--DROP TABLE #temptable
GO
/****** Object:  StoredProcedure [dbo].[UP_ArticleListGet]    Script Date: 10/16/2020 11:40:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ArticleTitle：文章主题
--Content：文章内容
--CatalogName：文章分类
--UserName：作者
--CRT_Time:发表时间
--RecordsTotal：总行数
--PaseIndex：页数
--PageSize：显示行数
--Replies：回复次数
--ViewTimes：点击次数
--UP_ArticleListGet '','','','','1','10','',''
--UP_ArticleListGet '','','','',1,16
CREATE  PROCEDURE [dbo].[UP_ArticleListGet](
	@CatalogID	INT=null,
	@ArticleTitle	NVARCHAR(50),	
	@UserID INT=null,
	@Content NVARCHAR(512),
	@PageNum INT,
	@PageSize INT,
	@OrderFields NVARCHAR(50)=null,
	@RecordsTotal INT=null OUTPUT
)
AS

DECLARE @SQL NVARCHAR(3000), @sqlWhere NVARCHAR(3000), @tempRecordsTotal INT,@topAddOne INT, @LocalOrderFields NVARCHAR(100)
IF @PageNum < 1 
	SET @PageNum = 1
SET @sqlWhere = N' WHERE art.IsActive = 1 '
IF @CatalogID > 0
	SET @sqlWhere = @sqlWhere + N' AND art.CatalogID = ''' + CAST(@CatalogID AS NVARCHAR(18)) + N''''
ELSE
BEGIN
IF @ArticleTitle <> ''
	SET @sqlWhere = @sqlWhere + N' AND art.ArticleTitle LIKE ''%' + @ArticleTitle + N'%'''
IF @UserID > 0
	SET @sqlWhere = @sqlWhere + N' AND art.UserID = ''' + CAST(@UserID AS NVARCHAR(18)) + N''''
IF @Content <> ''
	SET @sqlWhere = @sqlWhere + N' AND art.Content LIKE ''%' + @Content + N'%'''
END
SET @LocalOrderFields = @OrderFields
IF @OrderFields = ''	--直接写的...
	SET @LocalOrderFields = 'CRT_Time DESC'
SET @tempRecordsTotal = @PageNum * @PageSize
SET @topAddOne = (@PageNum - 1) * @PageSize
SET @SQL = N'
DECLARE @PageIndex INT 
SET @PageIndex = ' + CAST(@PageNum AS NVARCHAR(18)) + ' 
SET NOCOUNT ON
SELECT art.ArticleID,art.ArticleTitle as ArticleTitle, art.Content as Content, art.OrderNO as OrderNO, 
art.ViewTimes as ViewTimes, art.Replies as Replies,art.CRT_Time as CRT_Time,ctlg.CatalogName, u.UserName as UserName,
art.Year,art.Month,art.Day,t.TypeName
INTO #temptable  
FROM Guo_Acticle art LEFT JOIN  
Guo_User u ON art.UserID = u.ID LEFT JOIN  Guo_Type	t On art.TypeID = t.TypeID LEFT JOIN 
Guo_Catalog ctlg ON art.CatalogID = ctlg.CatalogID ' + @sqlWhere + ' ORDER BY ArticleID DESC
SELECT @rt = COUNT(1) FROM #temptable 
IF @rt < ' + CAST(@tempRecordsTotal AS NVARCHAR(18)) + ' 
SET @PageIndex = CONVERT(INT,ROUND(@rt / (' + CAST(@PageSize AS NVARCHAR(18)) + ' + 0.0),0)) 
--SET @topAddOne = (@PageIndex - 1) * ' + CAST(@PageSize AS NVARCHAR(18)) + ' 
SELECT * FROM (SELECT TOP ' + CAST(@PageSize AS NVARCHAR(18)) + ' * FROM #temptable  
WHERE ArticleID < 
	( 
		SELECT ISNULL(MIN(ArticleID),9999999) 
		FROM ( 
			SELECT TOP ' + CAST(@topAddOne AS NVARCHAR(18)) + ' ArticleID     
			FROM #temptable
			) AS T
	) ORDER BY ArticleID DESC)as T1 ORDER BY CRT_Time DESC --' + @LocalOrderFields + ' --去掉参数固定为时间排序。
--SELECT ISNULL(@rt,0) AS RecordsTotal,ISNULL(@PageIndex,0) AS PaseIndex
DROP TABLE #temptable
'
--PRINT @SQL
--EXEC(@SQL)
EXEC SP_EXECUTESQL @SQL,N'@rt INT OUTPUT',@RecordsTotal OUTPUT

--DROP TABLE #temptable
GO
/****** Object:  UserDefinedFunction [dbo].[StrToChar]    Script Date: 10/16/2020 11:40:50 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 对象:  用户定义的函数 dbo.StrToChar    脚本日期: 2020-8-21 08:57:56 ******/


Create    FUNCTION [dbo].[StrToChar](@strSource VARCHAR(8000))
RETURNS @Re TABLE (RowID INT IDENTITY(1, 1), value NCHAR(1))
AS
BEGIN
	DECLARE @Len SMALLINT, @i SMALLINT, @CHAR NCHAR(1)

	SET @Len = LEN(@strSource)
	SET @i = 0

	WHILE @i < @Len
	BEGIN
		SET @CHAR = SUBSTRING(@strSource, @i + 1, 1)

		INSERT @Re SELECT @CHAR

		SET @i = @i + 1
	END

	RETURN 

END
GO
/****** Object:  Table [dbo].[Guo_User]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LoginID] [nvarchar](50) NULL,
	[RoleId] [int] NULL,
	[UserName] [nvarchar](500) NULL,
	[PassWord] [nvarchar](500) NULL,
	[Status] [bit] NULL,
	[CreateOn] [datetime] NULL,
	[UpdateOn] [datetime] NULL,
	[CreateBy] [int] NULL,
	[UpdateBy] [int] NULL,
	[Gender] [int] NULL,
	[Phone] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[Remark] [nvarchar](500) NULL,
	[HeadShot] [nvarchar](500) NULL,
 CONSTRAINT [PK_Guo_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_User] ON
INSERT [dbo].[Guo_User] ([ID], [LoginID], [RoleId], [UserName], [PassWord], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy], [Gender], [Phone], [Email], [Remark], [HeadShot]) VALUES (1, N'admin', 1, N'郭清云', N'e10adc3949ba59abbe56e057f20f883e', 1, CAST(0x0000AC2D013E4AD0 AS DateTime), CAST(0x0000AC3600EDD18B AS DateTime), 0, 1, 1, N'12345678910', N'925627046@qq.com', N'最高权限', N'/Upload/img/head.jpg')
INSERT [dbo].[Guo_User] ([ID], [LoginID], [RoleId], [UserName], [PassWord], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy], [Gender], [Phone], [Email], [Remark], [HeadShot]) VALUES (2, N'myling', 2, N'毛不易', N'e10adc3949ba59abbe56e057f20f883e', 1, CAST(0x0000AC3A010E469D AS DateTime), CAST(0x0000AC3A0110223C AS DateTime), 1, 2, 1, N'12345678912', N'12345678912@qq.com', N'权限测试账号', N'/Upload/img/QQ图片20200918162959.jpg')
SET IDENTITY_INSERT [dbo].[Guo_User] OFF
/****** Object:  Table [dbo].[Guo_Type]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Type](
	[TypeID] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NULL,
	[UserID] [int] NULL,
	[UserName] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
	[CRT_Time] [datetime] NULL,
 CONSTRAINT [PK_Guo_Type] PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Type] ON
INSERT [dbo].[Guo_Type] ([TypeID], [TypeName], [UserID], [UserName], [IsActive], [CRT_Time]) VALUES (1, N'原创', 1, N'郭清云', 1, CAST(0x0000AC2B00C5C100 AS DateTime))
INSERT [dbo].[Guo_Type] ([TypeID], [TypeName], [UserID], [UserName], [IsActive], [CRT_Time]) VALUES (2, N'转载', 1, N'郭清云', 1, CAST(0x0000AC2B00E6B680 AS DateTime))
INSERT [dbo].[Guo_Type] ([TypeID], [TypeName], [UserID], [UserName], [IsActive], [CRT_Time]) VALUES (3, N'其他', 1, N'郭清云', 1, CAST(0x0000AC2B0136E9C0 AS DateTime))
SET IDENTITY_INSERT [dbo].[Guo_Type] OFF
/****** Object:  Table [dbo].[Guo_SysLog]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_SysLog](
	[RowId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](100) NOT NULL,
	[Time] [datetime] NOT NULL,
	[OP_Eid] [int] NOT NULL,
	[OP_Ename] [nvarchar](50) NOT NULL,
	[Ip] [nvarchar](50) NOT NULL,
	[IpAddressName] [nvarchar](500) NULL,
	[Describe] [nvarchar](500) NULL,
	[Results] [bit] NOT NULL,
 CONSTRAINT [PK_Guo_SysLog] PRIMARY KEY CLUSTERED 
(
	[RowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Guo_SysLog', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Guo_SysLog', @level2type=N'COLUMN',@level2name=N'Time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作人Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Guo_SysLog', @level2type=N'COLUMN',@level2name=N'OP_Eid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作人姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Guo_SysLog', @level2type=N'COLUMN',@level2name=N'OP_Ename'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户端Ip' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Guo_SysLog', @level2type=N'COLUMN',@level2name=N'Ip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Guo_SysLog', @level2type=N'COLUMN',@level2name=N'Describe'
GO
SET IDENTITY_INSERT [dbo].[Guo_SysLog] ON
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (7, N'Login', CAST(0x0000AC2F00FCD6D0 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (8, N'Login', CAST(0x0000AC2F0113E4A6 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (9, N'Login', CAST(0x0000AC2F0114BD83 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (10, N'Login', CAST(0x0000AC2F011817B3 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (11, N'Login', CAST(0x0000AC2F01189AD6 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (12, N'Login', CAST(0x0000AC2F0119DBEC AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (13, N'Login', CAST(0x0000AC2F011CCDAB AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (14, N'Login', CAST(0x0000AC2F011E3F3F AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (15, N'Login', CAST(0x0000AC2F0158E743 AS DateTime), 1, N'郭清云', N'192.168.1.3', N'SKY-20191011QBY', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (16, N'Login', CAST(0x0000AC2F016D7CE7 AS DateTime), 1, N'郭清云', N'192.168.1.3', N'SKY-20191011QBY', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (17, N'Login', CAST(0x0000AC3000994D16 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (18, N'Login', CAST(0x0000AC30009F1173 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (19, N'Login', CAST(0x0000AC30009F810B AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (20, N'Login', CAST(0x0000AC3000B8549B AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (21, N'Login', CAST(0x0000AC3000BA7DF8 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (22, N'Login', CAST(0x0000AC3000BBE426 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (23, N'Login', CAST(0x0000AC3000BE4A2B AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (24, N'Login', CAST(0x0000AC3000C1E57B AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (25, N'Login', CAST(0x0000AC3000EB2C64 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (26, N'Login', CAST(0x0000AC3000EDD04E AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (27, N'Login', CAST(0x0000AC300103BECF AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (28, N'Login', CAST(0x0000AC300105C753 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (29, N'Login', CAST(0x0000AC30010850F4 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (30, N'Login', CAST(0x0000AC30010D8170 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (31, N'Login', CAST(0x0000AC30011528C9 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (32, N'Login', CAST(0x0000AC3001193397 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (33, N'Login', CAST(0x0000AC30011BC3D5 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (34, N'Login', CAST(0x0000AC3200B7D4EC AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (35, N'Login', CAST(0x0000AC3200B94252 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (36, N'Login', CAST(0x0000AC3201020577 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (37, N'Login', CAST(0x0000AC3201021A3F AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (38, N'Login', CAST(0x0000AC320104A9B8 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (39, N'Login', CAST(0x0000AC320109395F AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (40, N'Login', CAST(0x0000AC32011C5627 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (41, N'Login', CAST(0x0000AC3201201D42 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (42, N'Login', CAST(0x0000AC320120DA29 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (43, N'Login', CAST(0x0000AC3201596E81 AS DateTime), 1, N'郭清云', N'192.168.1.3', N'SKY-20191011QBY', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (44, N'Login', CAST(0x0000AC320159CAE8 AS DateTime), 1, N'郭清云', N'192.168.1.3', N'SKY-20191011QBY', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (45, N'Login', CAST(0x0000AC32015B06CF AS DateTime), 1, N'郭清云', N'192.168.1.3', N'SKY-20191011QBY', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (46, N'Login', CAST(0x0000AC32015C4C2E AS DateTime), 1, N'郭清云', N'192.168.1.3', N'SKY-20191011QBY', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (47, N'Login', CAST(0x0000AC32015F1A6F AS DateTime), 1, N'郭清云', N'192.168.1.3', N'SKY-20191011QBY', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (48, N'Login', CAST(0x0000AC3300A83894 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (49, N'Login', CAST(0x0000AC330111A3F0 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (50, N'Login', CAST(0x0000AC3400F76F88 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (51, N'Login', CAST(0x0000AC3400F8A59E AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (52, N'Login', CAST(0x0000AC3400FA276D AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (53, N'Login', CAST(0x0000AC3400FCE297 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (54, N'Login', CAST(0x0000AC3400FE3026 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (55, N'Login', CAST(0x0000AC3400FFC7C0 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (56, N'Login', CAST(0x0000AC340102330E AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (57, N'Login', CAST(0x0000AC340104BA8D AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (58, N'Login', CAST(0x0000AC3401063B60 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (59, N'Login', CAST(0x0000AC34010D4AD9 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (60, N'Login', CAST(0x0000AC34010EF05D AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (61, N'Login', CAST(0x0000AC3600BCB404 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (62, N'Login', CAST(0x0000AC3600BE9FEB AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (63, N'Login', CAST(0x0000AC3600C0C773 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (64, N'Login', CAST(0x0000AC3600EB601E AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (65, N'Login', CAST(0x0000AC3600ED6D6A AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (66, N'Login', CAST(0x0000AC3600FB74F8 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (67, N'Login', CAST(0x0000AC36011A7EC4 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (68, N'Login', CAST(0x0000AC36011F4F5C AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (69, N'Login', CAST(0x0000AC3700B699B4 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (70, N'Login', CAST(0x0000AC3700B6A85C AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (71, N'Login', CAST(0x0000AC3700B6C36D AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (72, N'Login', CAST(0x0000AC3700B7CF1A AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (73, N'Login', CAST(0x0000AC3700B95A53 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (74, N'Login', CAST(0x0000AC3700B9D2C4 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (75, N'Login', CAST(0x0000AC3700BA32DD AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (76, N'Login', CAST(0x0000AC3700BA927C AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (77, N'Login', CAST(0x0000AC3700BB04AC AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (78, N'Login', CAST(0x0000AC3700BB7C6E AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (79, N'Login', CAST(0x0000AC3700BD219E AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (80, N'Login', CAST(0x0000AC3700BDAAA0 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (81, N'Login', CAST(0x0000AC3700BDDD49 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (82, N'Login', CAST(0x0000AC3700BFA0CD AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (83, N'Login', CAST(0x0000AC3700BFBE85 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (84, N'Login', CAST(0x0000AC3700F24096 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (85, N'Login', CAST(0x0000AC3700FA1BBB AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (86, N'Login', CAST(0x0000AC37010A6B45 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (87, N'Login', CAST(0x0000AC370115C071 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (88, N'Login', CAST(0x0000AC37011AF089 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (89, N'Login', CAST(0x0000AC3800A0C5E8 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (90, N'Login', CAST(0x0000AC3800A81C72 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (91, N'Login', CAST(0x0000AC3800B2B5A8 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (92, N'Login', CAST(0x0000AC3800C15090 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (93, N'Login', CAST(0x0000AC3800FA1A73 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (94, N'Login', CAST(0x0000AC380103B56C AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (95, N'Login', CAST(0x0000AC380111310B AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (96, N'Login', CAST(0x0000AC3801116A7A AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (97, N'Login', CAST(0x0000AC3801121B56 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (98, N'Login', CAST(0x0000AC3801123ABE AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (99, N'Login', CAST(0x0000AC3801224E2A AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (100, N'Login', CAST(0x0000AC3901113F1F AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (101, N'Login', CAST(0x0000AC39011E5494 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (102, N'Login', CAST(0x0000AC3A009BD674 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (103, N'Login', CAST(0x0000AC3A009D2FB9 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (104, N'Login', CAST(0x0000AC3A00A32123 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (105, N'Login', CAST(0x0000AC3A00A788B6 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (106, N'Login', CAST(0x0000AC3A00AED2F5 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
GO
print 'Processed 100 total records'
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (107, N'Login', CAST(0x0000AC3A00B1ED0B AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (108, N'Login', CAST(0x0000AC3A00B3894B AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (109, N'Login', CAST(0x0000AC3A00B4F47F AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (110, N'Login', CAST(0x0000AC3A00B85484 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (111, N'Login', CAST(0x0000AC3A00BB8ECF AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (112, N'Login', CAST(0x0000AC3A00BF7DD4 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (113, N'Login', CAST(0x0000AC3A00EDFC9B AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (114, N'Login', CAST(0x0000AC3A00F01060 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (115, N'Login', CAST(0x0000AC3A00F227C6 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (116, N'Login', CAST(0x0000AC3A00F3EBDF AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (117, N'Login', CAST(0x0000AC3A00F9E2FA AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (118, N'Login', CAST(0x0000AC3A00FC86A3 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (119, N'Login', CAST(0x0000AC3A00FFD022 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (120, N'Login', CAST(0x0000AC3A0102E4E6 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (121, N'Login', CAST(0x0000AC3A0104B0F3 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (122, N'Login', CAST(0x0000AC3A01080949 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (123, N'Login', CAST(0x0000AC3A010C51C3 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (124, N'Login', CAST(0x0000AC3A010E5E34 AS DateTime), 2, N'毛不易', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (125, N'Login', CAST(0x0000AC3A010F3979 AS DateTime), 2, N'毛不易', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (126, N'Login', CAST(0x0000AC3A010F4673 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (127, N'Login', CAST(0x0000AC3A010F67DD AS DateTime), 2, N'毛不易', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (128, N'Login', CAST(0x0000AC3A011035E8 AS DateTime), 2, N'毛不易', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (129, N'Login', CAST(0x0000AC3A0110B071 AS DateTime), 2, N'毛不易', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (130, N'Login', CAST(0x0000AC3A0110BB1E AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (131, N'Login', CAST(0x0000AC3B009D3EC6 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (132, N'Login', CAST(0x0000AC3B00A43E23 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (133, N'Login', CAST(0x0000AC3B00B6D3EC AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (134, N'Login', CAST(0x0000AC3D00AF5E6D AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (135, N'Login', CAST(0x0000AC3D00B37F98 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (136, N'Login', CAST(0x0000AC3D00B9D369 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (138, N'Login', CAST(0x0000AC3E00FDC030 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (139, N'Login', CAST(0x0000AC3F011F096B AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (140, N'Login', CAST(0x0000AC41011059BB AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (141, N'Login', CAST(0x0000AC4101158AFD AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (142, N'Login', CAST(0x0000AC410116A898 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (143, N'Login', CAST(0x0000AC4200A0A0C6 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (144, N'Login', CAST(0x0000AC4200B21FAA AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (145, N'Login', CAST(0x0000AC4200C069B7 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (146, N'Login', CAST(0x0000AC4200F5806A AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (147, N'Login', CAST(0x0000AC4201020F74 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (148, N'Login', CAST(0x0000AC42012094B5 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (149, N'Login', CAST(0x0000AC4300A4FCA2 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (150, N'Login', CAST(0x0000AC4300B578A5 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (151, N'Login', CAST(0x0000AC4300BAB187 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (152, N'Login', CAST(0x0000AC4300BEA5C2 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (153, N'Login', CAST(0x0000AC4300F949E5 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (154, N'Login', CAST(0x0000AC4301001744 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (155, N'Login', CAST(0x0000AC4401144347 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (156, N'Login', CAST(0x0000AC4500BAAC27 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (157, N'Login', CAST(0x0000AC4500FAE3F6 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (158, N'Login', CAST(0x0000AC45011E29B8 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (159, N'Login', CAST(0x0000AC4600A5DFD5 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (160, N'Login', CAST(0x0000AC4600A6E0C5 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (161, N'Login', CAST(0x0000AC4600AEA615 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (162, N'Login', CAST(0x0000AC4600B3258D AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (163, N'Login', CAST(0x0000AC4600B64BAD AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (164, N'Login', CAST(0x0000AC4600B7BA88 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (165, N'Login', CAST(0x0000AC4600BD2708 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (166, N'Login', CAST(0x0000AC50010B306E AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (167, N'Login', CAST(0x0000AC5300AAC013 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (168, N'Login', CAST(0x0000AC5300FA805F AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (169, N'Login', CAST(0x0000AC5500AE031E AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (170, N'Login', CAST(0x0000AC5500B46218 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (171, N'Login', CAST(0x0000AC5500BD3C3F AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (172, N'Login', CAST(0x0000AC5600B50AEC AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (173, N'Login', CAST(0x0000AC5600B5DED2 AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (174, N'Login', CAST(0x0000AC5600B6833E AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
INSERT [dbo].[Guo_SysLog] ([RowId], [Type], [Time], [OP_Eid], [OP_Ename], [Ip], [IpAddressName], [Describe], [Results]) VALUES (175, N'Login', CAST(0x0000AC5600BA5E6A AS DateTime), 1, N'郭清云', N'192.168.168.229', N'SKY-20190812FIH', N'登录成功', 1)
SET IDENTITY_INSERT [dbo].[Guo_SysLog] OFF
/****** Object:  Table [dbo].[Guo_Sys_Menu]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Guo_Sys_Menu](
	[MenuID] [smallint] IDENTITY(1,1) NOT NULL,
	[LFT] [smallint] NULL,
	[RGT] [smallint] NULL,
	[LVL] [smallint] NULL,
	[REL] [smallint] NOT NULL,
	[Caption] [varchar](50) NOT NULL,
	[URL] [varchar](255) NULL,
	[IsSystem] [bit] NULL,
	[IsHid] [bit] NULL,
	[BlockID] [smallint] NULL,
	[Par_ID] [int] NOT NULL,
	[icon] [nvarchar](100) NULL,
 CONSTRAINT [PK_Guo_Sys_Menu] PRIMARY KEY CLUSTERED 
(
	[MenuID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Guo_Sys_Menu] ON
INSERT [dbo].[Guo_Sys_Menu] ([MenuID], [LFT], [RGT], [LVL], [REL], [Caption], [URL], [IsSystem], [IsHid], [BlockID], [Par_ID], [icon]) VALUES (1, 1, 2, 1, 1, N'博客', N'/Home/Index', 1, 1, NULL, 0, NULL)
INSERT [dbo].[Guo_Sys_Menu] ([MenuID], [LFT], [RGT], [LVL], [REL], [Caption], [URL], [IsSystem], [IsHid], [BlockID], [Par_ID], [icon]) VALUES (2, 3, 4, 1, 1, N'留言', N'/Home/Message', 1, 1, NULL, 0, NULL)
INSERT [dbo].[Guo_Sys_Menu] ([MenuID], [LFT], [RGT], [LVL], [REL], [Caption], [URL], [IsSystem], [IsHid], [BlockID], [Par_ID], [icon]) VALUES (3, 5, 6, 1, 1, N'相册', N'/Home/Photo', 1, 1, NULL, 0, NULL)
INSERT [dbo].[Guo_Sys_Menu] ([MenuID], [LFT], [RGT], [LVL], [REL], [Caption], [URL], [IsSystem], [IsHid], [BlockID], [Par_ID], [icon]) VALUES (4, 7, 8, 1, 1, N'日记', N'/Home/Diary', 1, 1, NULL, 0, NULL)
INSERT [dbo].[Guo_Sys_Menu] ([MenuID], [LFT], [RGT], [LVL], [REL], [Caption], [URL], [IsSystem], [IsHid], [BlockID], [Par_ID], [icon]) VALUES (5, 9, 10, 1, 1, N'有链', N'/Home/FriendUrl', 1, 1, NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[Guo_Sys_Menu] OFF
/****** Object:  Table [dbo].[Guo_Role]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LoginID] [nvarchar](50) NULL,
	[UserName] [nvarchar](500) NULL,
	[Remark] [nvarchar](500) NULL,
	[Status] [bit] NULL,
	[CreateOn] [datetime] NULL,
	[UpdateOn] [datetime] NULL,
	[CreateBy] [int] NULL,
	[UpdateBy] [int] NULL,
 CONSTRAINT [PK_Guo_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Role] ON
INSERT [dbo].[Guo_Role] ([Id], [LoginID], [UserName], [Remark], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (1, N'admin', N'超级管理员', N'陌上人如玉,公子世无双', 1, CAST(0x0000AC2D013E4AD0 AS DateTime), CAST(0x0000AC3A010DEC6C AS DateTime), 0, 1)
INSERT [dbo].[Guo_Role] ([Id], [LoginID], [UserName], [Remark], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (2, N'ptusers', N'普通用户', N'平平无奇的用户', 1, CAST(0x0000AC3A009C16DD AS DateTime), CAST(0x0000AC5600B707CE AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[Guo_Role] OFF
/****** Object:  Table [dbo].[Guo_Resources]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Resources](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[UserID] [int] NULL,
	[OrderNo] [int] NULL,
	[CRT_Time] [datetime] NULL,
 CONSTRAINT [PK_Guo_Resources] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Resources] ON
INSERT [dbo].[Guo_Resources] ([Id], [Name], [UserID], [OrderNo], [CRT_Time]) VALUES (1, N'静态模板', 1, 1, CAST(0x0000AC3E010DFD30 AS DateTime))
INSERT [dbo].[Guo_Resources] ([Id], [Name], [UserID], [OrderNo], [CRT_Time]) VALUES (2, N'C#', 1, 2, CAST(0x0000AC3E011E77F0 AS DateTime))
INSERT [dbo].[Guo_Resources] ([Id], [Name], [UserID], [OrderNo], [CRT_Time]) VALUES (3, N'JavaScript', 1, 3, CAST(0x0000AC3E011EBE40 AS DateTime))
SET IDENTITY_INSERT [dbo].[Guo_Resources] OFF
/****** Object:  Table [dbo].[Guo_QQUser]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_QQUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OpenId] [nvarchar](100) NULL,
	[NickName] [nvarchar](100) NULL,
	[Gender] [int] NULL,
	[HeadShot] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[Status] [bit] NULL,
	[LastLogin] [datetime] NULL,
	[CreateOn] [datetime] NULL,
 CONSTRAINT [PK_Guo_QQUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_QQUser] ON
INSERT [dbo].[Guo_QQUser] ([Id], [OpenId], [NickName], [Gender], [HeadShot], [Email], [Status], [LastLogin], [CreateOn]) VALUES (1, N'5DB22905E4DA3582E221CE85737059C1', N'蚯蚓', 1, N'http://thirdqq.qlogo.cn/g?b=oidb&k=q8BmhyWUTZEpo7Us6QTqhA&s=140&t=1557712926', N'', 1, CAST(0x0000A939013779E4 AS DateTime), CAST(0x0000AA990167C400 AS DateTime))
INSERT [dbo].[Guo_QQUser] ([Id], [OpenId], [NickName], [Gender], [HeadShot], [Email], [Status], [LastLogin], [CreateOn]) VALUES (4, N'7AE01F1862D52C452B296158F17AF25E', N'小小小蚯蚓', 1, N'https://thirdqq.qlogo.cn/g?b=oidb&k=TI6MEZXl7gGtkTribiah3icvA&s=140&t=1552461754', N'', 1, CAST(0x0000AA9901696080 AS DateTime), CAST(0x0000AA9901696080 AS DateTime))
INSERT [dbo].[Guo_QQUser] ([Id], [OpenId], [NickName], [Gender], [HeadShot], [Email], [Status], [LastLogin], [CreateOn]) VALUES (5, N'2D8289F875ECA7FF64C388A336E7D2DC', N'--', 1, N'https://thirdqq.qlogo.cn/g?b=oidb&k=VFjtC0gWn9YLqQaoBOfWEw&s=100&t=1557278201', N'', 1, CAST(0x0000AA9A00A8843C AS DateTime), CAST(0x0000AA9A00A8843C AS DateTime))
INSERT [dbo].[Guo_QQUser] ([Id], [OpenId], [NickName], [Gender], [HeadShot], [Email], [Status], [LastLogin], [CreateOn]) VALUES (6, N'3AC542CEB2BDB3261F98691E1F851CC8', N'莫ღ汐白丶', 1, N'https://thirdqq.qlogo.cn/g?b=oidb&k=4dfr86UrnVsAbaYxZ4IYOA&s=140&t=1556771291', N'', 1, CAST(0x0000AA9A00A8A50C AS DateTime), CAST(0x0000AA9A00A8A50C AS DateTime))
INSERT [dbo].[Guo_QQUser] ([Id], [OpenId], [NickName], [Gender], [HeadShot], [Email], [Status], [LastLogin], [CreateOn]) VALUES (7, N'E3133C803C5DACA8AB13F000769E5787', N'試毅-思伟✅已认证', 1, N'https://thirdqq.qlogo.cn/g?b=oidb&k=dtcicI20RLTIMFQlqJ3txuA&s=140&t=1557096682', N'', 1, CAST(0x0000AA9B00C17370 AS DateTime), CAST(0x0000AA9B00C17370 AS DateTime))
INSERT [dbo].[Guo_QQUser] ([Id], [OpenId], [NickName], [Gender], [HeadShot], [Email], [Status], [LastLogin], [CreateOn]) VALUES (8, N'9587B49F81E6C2141B1128288F13EA1E', N'洋芋', 1, N'http://thirdqq.qlogo.cn/g?b=oidb&k=9yqHfWPhCjxice8ZtqETsUg&s=140&t=1556092932', N'', 1, CAST(0x0000AC0C00B4792C AS DateTime), CAST(0x0000AA9E00B4792C AS DateTime))
INSERT [dbo].[Guo_QQUser] ([Id], [OpenId], [NickName], [Gender], [HeadShot], [Email], [Status], [LastLogin], [CreateOn]) VALUES (9, N'ED7CE034D044B3759ED7D78EF15F837F', N'沧海大叔', 1, N'http://thirdqq.qlogo.cn/g?b=oidb&k=rVvtgxNp4qTTmR90kPI5hA&s=140&t=1555941697', N'', 1, CAST(0x0000AAA4011F3820 AS DateTime), CAST(0x0000AAA4011F3820 AS DateTime))
INSERT [dbo].[Guo_QQUser] ([Id], [OpenId], [NickName], [Gender], [HeadShot], [Email], [Status], [LastLogin], [CreateOn]) VALUES (10, N'B3428905A4D64E98E30A88327EC9EE78', N'　　', 1, N'http://thirdqq.qlogo.cn/g?b=oidb&k=XmqTeI956qzffrericomrKQ&s=640&t=1594643451', N'', 1, CAST(0x0000AC52011F35A3 AS DateTime), CAST(0x0000AC52011F35A3 AS DateTime))
SET IDENTITY_INSERT [dbo].[Guo_QQUser] OFF
/****** Object:  Table [dbo].[Guo_Menu_Action]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Menu_Action](
	[MenuId] [int] NOT NULL,
	[ActionId] [int] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (2, 1)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (2, 2)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (2, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (2, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (3, 1)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (3, 2)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (3, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (3, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (3, 6)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (4, 1)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (4, 2)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (4, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (4, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (4, 5)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (5, 1)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (5, 2)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (5, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (5, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (10, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (10, 7)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (12, 1)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (12, 2)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (12, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (12, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (13, 1)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (13, 2)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (13, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (13, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (14, 1)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (14, 2)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (14, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (14, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (15, 1)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (15, 2)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (15, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (15, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (16, 1)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (16, 2)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (16, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (16, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (17, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (17, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (18, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (18, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (19, 2)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (19, 3)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (19, 4)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (2, 8)
INSERT [dbo].[Guo_Menu_Action] ([MenuId], [ActionId]) VALUES (12, 8)
/****** Object:  Table [dbo].[Guo_Links]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Links](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Url] [nvarchar](500) NULL,
	[Icon] [nvarchar](500) NULL,
	[Describe] [nvarchar](500) NULL,
	[CreateOn] [datetime] NULL,
 CONSTRAINT [PK_Guo_Links] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Links] ON
INSERT [dbo].[Guo_Links] ([Id], [Name], [Url], [Icon], [Describe], [CreateOn]) VALUES (1, N'一款简洁优雅的hexo主题', N'https://zhousiwei.gitee.io', N'https://zhousiwei.gitee.io/img/head.jpg', N'一个简洁优雅的hexo主题 A simple and elegant theme for hexo.可以快速使用上手。（我徒弟，多多支持）', CAST(0x0000AA9800B7C294 AS DateTime))
INSERT [dbo].[Guo_Links] ([Id], [Name], [Url], [Icon], [Describe], [CreateOn]) VALUES (2, N'燕十三', N'https://www.yanshisan.cn', N'https://www.yanshisan.cn/logo.png', N'剑气纵横三万里，一剑光寒十九洲。', CAST(0x0000AA990088746C AS DateTime))
INSERT [dbo].[Guo_Links] ([Id], [Name], [Url], [Icon], [Describe], [CreateOn]) VALUES (3, N'Mr.LUCK1', N'http://www.zhyjohn.cn', N'https://www.zhyjohn.cn/image/head.jpg', N'想法总是美好的，现实往往都是残酷的', CAST(0x0000AA9A00A48FBC AS DateTime))
SET IDENTITY_INSERT [dbo].[Guo_Links] OFF
/****** Object:  Table [dbo].[Guo_Feedback]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Feedback](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SendId] [int] NULL,
	[AcceptId] [int] NULL,
	[Content] [nvarchar](500) NULL,
	[ParentId] [int] NULL,
	[City] [nvarchar](50) NULL,
	[Equip] [nvarchar](50) NULL,
	[CreateOn] [datetime] NULL,
 CONSTRAINT [PK_Guo_Feedback] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Feedback] ON
INSERT [dbo].[Guo_Feedback] ([Id], [SendId], [AcceptId], [Content], [ParentId], [City], [Equip], [CreateOn]) VALUES (9, 10, 0, N'哈<img src="http://localhost:13174/Content/layui/images/face/2.gif" alt="[哈哈]">', 0, N'重庆市', N'Chrome浏览器', CAST(0x0000AC5300A97612 AS DateTime))
INSERT [dbo].[Guo_Feedback] ([Id], [SendId], [AcceptId], [Content], [ParentId], [City], [Equip], [CreateOn]) VALUES (10, 10, 10, N'哈哈？', 9, N'重庆市', N'Chrome浏览器', CAST(0x0000AC5300A9FCEB AS DateTime))
INSERT [dbo].[Guo_Feedback] ([Id], [SendId], [AcceptId], [Content], [ParentId], [City], [Equip], [CreateOn]) VALUES (11, 10, 10, N'继续回复测试滴滴', 9, N'重庆市', N'Chrome浏览器', CAST(0x0000AC5300AA2064 AS DateTime))
INSERT [dbo].[Guo_Feedback] ([Id], [SendId], [AcceptId], [Content], [ParentId], [City], [Equip], [CreateOn]) VALUES (12, 10, 0, N'111111', 9, N'成都市', N'IE浏览器', CAST(0x0000AC5300CB15E4 AS DateTime))
INSERT [dbo].[Guo_Feedback] ([Id], [SendId], [AcceptId], [Content], [ParentId], [City], [Equip], [CreateOn]) VALUES (13, 10, 0, N'项目开源哦', 0, N'重庆市', N'Chrome浏览器', CAST(0x0000AC5200A97612 AS DateTime))
SET IDENTITY_INSERT [dbo].[Guo_Feedback] OFF
/****** Object:  Table [dbo].[Guo_Diays]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Diays](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Content] [text] NULL,
	[CreateOn] [datetime] NULL,
 CONSTRAINT [PK_Guo_Diays] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Diays] ON
INSERT [dbo].[Guo_Diays] ([Id], [Content], [CreateOn]) VALUES (5, N'<p>今天是个好日子！</p>', CAST(0x0000AC32011CD8E6 AS DateTime))
INSERT [dbo].[Guo_Diays] ([Id], [Content], [CreateOn]) VALUES (6, N'哈哈哈完成了日志模块！<img src="http://localhost:13174/Areas/Admin/plugins/layui/images/face/1.gif" alt="[嘻嘻]">', CAST(0x0000AC32011CFD1D AS DateTime))
INSERT [dbo].[Guo_Diays] ([Id], [Content], [CreateOn]) VALUES (7, N'今天完善了，文章分类、文章类型以及菜单管理模块<img src="http://localhost:13174/Areas/Admin/plugins/layui/images/face/45.gif" alt="[怒骂]">', CAST(0x0000AC3301121C79 AS DateTime))
INSERT [dbo].[Guo_Diays] ([Id], [Content], [CreateOn]) VALUES (8, N'今天搞完了后台管理的系统设置模块，以及留言管理', CAST(0x0000AC3600FC8753 AS DateTime))
INSERT [dbo].[Guo_Diays] ([Id], [Content], [CreateOn]) VALUES (9, N'想接入QQ互联，方便登录留言，但是得要网站发布后了才能申请<img src="http://localhost:13174/Areas/Admin/plugins/layui/images/face/48.gif" alt="[伤心]">', CAST(0x0000AC3600FCD191 AS DateTime))
INSERT [dbo].[Guo_Diays] ([Id], [Content], [CreateOn]) VALUES (10, N'今天也仍然是改BUG的一天<img src="http://localhost:13174/Areas/Admin/plugins/layui/images/face/12.gif" alt="[泪]">', CAST(0x0000AC380122ABB7 AS DateTime))
INSERT [dbo].[Guo_Diays] ([Id], [Content], [CreateOn]) VALUES (11, N'<p>项目开源啦<img src="http://localhost:13174/Areas/Admin/plugins/layui/images/face/1.gif" alt="[嘻嘻]">，项目地址：<a href="https://gitee.com/guo_zi_shan/yun-q_-blogs">https://gitee.com/guo_zi_shan/yun-q_-blogs</a></p>', CAST(0x0000AC5500B045EB AS DateTime))
INSERT [dbo].[Guo_Diays] ([Id], [Content], [CreateOn]) VALUES (12, N'如果在使用中，亦或是学习中有什么问题，我希望您在极尽思考后，再来询问我。希望您记住一句话，授人以鱼不如授人以渔。如果自己思考下实在没解决，我一定尽力为您解决。感谢理解<img src="http://localhost:13174/Areas/Admin/plugins/layui/images/face/47.gif" alt="[心]">', CAST(0x0000AC5500B0C2B9 AS DateTime))
SET IDENTITY_INSERT [dbo].[Guo_Diays] OFF
/****** Object:  Table [dbo].[Guo_DemoType]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_DemoType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[UserID] [int] NULL,
	[OrderNo] [int] NULL,
	[CRT_Time] [datetime] NULL,
 CONSTRAINT [PK_Guo_DemoType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_DemoType] ON
INSERT [dbo].[Guo_DemoType] ([Id], [Name], [UserID], [OrderNo], [CRT_Time]) VALUES (1, N'静态模板', 1, 1, CAST(0x0000AC3E010DFD30 AS DateTime))
INSERT [dbo].[Guo_DemoType] ([Id], [Name], [UserID], [OrderNo], [CRT_Time]) VALUES (2, N'响应式', 1, 2, CAST(0x0000AC3E011E77F0 AS DateTime))
SET IDENTITY_INSERT [dbo].[Guo_DemoType] OFF
/****** Object:  Table [dbo].[Guo_Demo]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Demo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypedmId] [int] NULL,
	[ResourId] [int] NULL,
	[Title] [nvarchar](50) NULL,
	[ImgUrl] [nvarchar](500) NULL,
	[Content] [ntext] NULL,
	[Abstract] [nvarchar](500) NULL,
	[CRT_Time] [datetime] NULL,
	[Up_Time] [datetime] NULL,
	[UserID] [int] NULL,
	[ViewTimes] [int] NULL,
	[Replies] [int] NULL,
	[IsTop] [bit] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Guo_Demo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Demo] ON
INSERT [dbo].[Guo_Demo] ([Id], [TypedmId], [ResourId], [Title], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive]) VALUES (1, 1, 1, N'个人博客模板', N'/Upload/img/bg2.jpg', N'一款整体采用Layui制作，大气简洁的个人博客，IT技术博客交流网站模板html下载。包含：首页、博客、留言、日记、友链等总共6个页面。', N'一款整体采用Layui制作，大气简洁的个人博客，IT技术博客交流网站模板html下载。包含：首页、博客、留言、日记、友链等总共6个页面。', CAST(0x0000AC3E01137B70 AS DateTime), NULL, 1, 20, 0, 0, 1)
INSERT [dbo].[Guo_Demo] ([Id], [TypedmId], [ResourId], [Title], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive]) VALUES (2, 1, 1, N'互联网技术官方网站响应式模板', N'/Upload/img/bg1.jpg', N'一款整体采用Layui制作，大气简洁的个人博客，IT技术博客交流网站模板html下载。包含：首页、博客、留言、日记、友链等总共6个页面。', N'一款整体采用Layui制作，大气简洁的个人博客，IT技术博客交流网站模板html下载。包含：首页、博客、留言、日记、友链等总共6个页面。', CAST(0x0000AC3E01137B70 AS DateTime), NULL, 1, 20, 0, 0, 1)
SET IDENTITY_INSERT [dbo].[Guo_Demo] OFF
/****** Object:  Table [dbo].[Guo_Comment]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Comment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SendId] [int] NULL,
	[AcceptId] [int] NULL,
	[Content] [nvarchar](500) NULL,
	[Status] [bit] NULL,
	[ParentId] [int] NULL,
	[ArticleId] [int] NULL,
	[CreateOn] [datetime] NULL,
 CONSTRAINT [PK_Guo_Comment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Comment] ON
INSERT [dbo].[Guo_Comment] ([Id], [SendId], [AcceptId], [Content], [Status], [ParentId], [ArticleId], [CreateOn]) VALUES (1, 10, 0, N'哈哈', 1, 0, 17, CAST(0x0000AC5300C285AA AS DateTime))
INSERT [dbo].[Guo_Comment] ([Id], [SendId], [AcceptId], [Content], [Status], [ParentId], [ArticleId], [CreateOn]) VALUES (2, 10, 10, N'哈个锤子', 1, 1, 17, CAST(0x0000AC5300C5C729 AS DateTime))
SET IDENTITY_INSERT [dbo].[Guo_Comment] OFF
/****** Object:  Table [dbo].[Guo_CatalogReplys]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Guo_CatalogReplys](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ArticleID] [int] NULL,
	[ReplyerID] [int] NULL,
	[ReceiverIDs] [varchar](4000) NULL,
	[Content] [nvarchar](4000) NULL,
	[LVL] [int] NULL,
	[RplTime] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Guo_CatalogReplys] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Guo_Catalog]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Catalog](
	[CatalogID] [int] IDENTITY(1,1) NOT NULL,
	[CatalogName] [nvarchar](50) NULL,
	[UserID] [int] NULL,
	[UserName] [nvarchar](100) NULL,
	[OrderNo] [int] NULL,
	[IsActive] [bit] NULL,
	[CRT_Time] [datetime] NULL,
 CONSTRAINT [PK_Guo_Catalog] PRIMARY KEY CLUSTERED 
(
	[CatalogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Catalog] ON
INSERT [dbo].[Guo_Catalog] ([CatalogID], [CatalogName], [UserID], [UserName], [OrderNo], [IsActive], [CRT_Time]) VALUES (1, N'C#', 1, N'郭清云                                                                                                 ', 1, 1, CAST(0x0000AC2A00000000 AS DateTime))
INSERT [dbo].[Guo_Catalog] ([CatalogID], [CatalogName], [UserID], [UserName], [OrderNo], [IsActive], [CRT_Time]) VALUES (2, N'版本控制', 1, N'郭清云                                                                                                 ', 2, 1, CAST(0x0000AC2A00000000 AS DateTime))
INSERT [dbo].[Guo_Catalog] ([CatalogID], [CatalogName], [UserID], [UserName], [OrderNo], [IsActive], [CRT_Time]) VALUES (3, N'前端', 1, N'郭清云                                                                                                 ', 3, 1, CAST(0x0000AC2A00000000 AS DateTime))
INSERT [dbo].[Guo_Catalog] ([CatalogID], [CatalogName], [UserID], [UserName], [OrderNo], [IsActive], [CRT_Time]) VALUES (4, N'其他', 1, N'郭清云                                                                                                 ', 4, 1, CAST(0x0000AC2C00000000 AS DateTime))
INSERT [dbo].[Guo_Catalog] ([CatalogID], [CatalogName], [UserID], [UserName], [OrderNo], [IsActive], [CRT_Time]) VALUES (5, N'项目推荐', 1, N'郭清云', 5, NULL, CAST(0x0000AC5600B603B1 AS DateTime))
SET IDENTITY_INSERT [dbo].[Guo_Catalog] OFF
/****** Object:  Table [dbo].[Guo_AdminMenu]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_AdminMenu](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MenuName] [nvarchar](100) NULL,
	[MenuUrl] [nvarchar](100) NULL,
	[MenuIcon] [nvarchar](100) NULL,
	[OrderNo] [int] NULL,
	[ParentId] [int] NULL,
	[Status] [bit] NULL,
	[CreateOn] [datetime] NULL,
	[UpdateOn] [datetime] NULL,
	[CreateBy] [int] NULL,
	[UpdateBy] [int] NULL,
 CONSTRAINT [PK_Guo_AdminMenu] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_AdminMenu] ON
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (1, N'权限管理', N'', N'icon-setting-permissions', 1, 0, 1, CAST(0x0000AA0200F81498 AS DateTime), CAST(0x0000AA6F011B497C AS DateTime), 0, 1)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (2, N'菜单管理', N'/permissions/menu', N'icon-caidan', 1, 1, 1, CAST(0x0000AA0200F81BA0 AS DateTime), CAST(0x0000AA0200F81F24 AS DateTime), 0, 0)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (3, N'角色管理', N'/permissions/role', N'icon-jiaoseguanli', 2, 1, 1, CAST(0x0000AA0200F8217C AS DateTime), CAST(0x0000AA0200F8262C AS DateTime), 0, 0)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (4, N'用户管理', N'/permissions/user', N'icon-yonghu', 3, 1, 1, CAST(0x0000AA0200F829B0 AS DateTime), CAST(0x0000AA0200F82D34 AS DateTime), 0, 0)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (5, N'操作管理', N'/permissions/action', N'icon-shezhi', 4, 1, 1, CAST(0x0000AA0200F831E4 AS DateTime), CAST(0x0000AA0200F83694 AS DateTime), 0, 0)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (6, N'系统设置', N'', N'icon-xitong', 2, 0, 1, CAST(0x0000AA0200F83A18 AS DateTime), CAST(0x0000AA0200F83C70 AS DateTime), 0, 0)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (7, N'网站设置', N'/sysset/website', N'icon-ditu', 1, 6, 1, CAST(0x0000AA0200F83FF4 AS DateTime), CAST(0x0000AA0200F8424C AS DateTime), 0, 0)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (8, N'基本资料', N'/SysSet/info', N'icon-jibenziliao', 2, 6, 1, CAST(0x0000AA0200F845D0 AS DateTime), CAST(0x0000AA0200F84828 AS DateTime), 0, 0)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (9, N'修改密码', N'/SysSet/password', N'icon-xiugaimima', 3, 6, 1, CAST(0x0000AA0200F84CD8 AS DateTime), CAST(0x0000AA0200F8505C AS DateTime), 0, 0)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (10, N'日志管理', N'/SysSet/Log', N'icon-xitongrizhi', 4, 6, 1, CAST(0x0000AA0200F852B4 AS DateTime), CAST(0x0000AA0200F85638 AS DateTime), 0, 0)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (11, N'博客管理', NULL, N'icon-zhuye', 0, 0, 1, CAST(0x0000AA7600ED89C4 AS DateTime), CAST(0x0000AA0200F85638 AS DateTime), 1, 0)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (12, N'文章类型', N'/Blog/ArticleType', N'icon-jibenziliao', 7, 11, 1, CAST(0x0000AA7600EFA4D4 AS DateTime), CAST(0x0000AA7B01238CB8 AS DateTime), 1, 1)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (13, N'文章分类', N'/Blog/ArticleClass', N'icon-jibenziliao', 6, 11, 1, CAST(0x0000AA7600F4BE4C AS DateTime), CAST(0x0000AA77012A7D48 AS DateTime), 1, 1)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (14, N'友情链接', N'/Blog/Links', N'icon-jibenziliao', 4, 11, 1, CAST(0x0000AA7600F5F85C AS DateTime), CAST(0x0000AA7B012C1644 AS DateTime), 1, 1)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (15, N'日记管理', N'/Blog/Diarys', N'icon-jibenziliao', 3, 11, 1, CAST(0x0000AA7600F76020 AS DateTime), CAST(0x0000AA7B01807644 AS DateTime), 1, 1)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (16, N'文章管理', N'/Blog/Article', N'icon-xitongrizhi', 0, 11, 1, CAST(0x0000AA7600F7D1CC AS DateTime), CAST(0x0000AC37011B2644 AS DateTime), 1, 1)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (17, N'评论管理', N'/Blog/Comment', N'icon-jibenziliao', 1, 11, 1, CAST(0x0000AA770103A970 AS DateTime), CAST(0x0000AA7D00B85510 AS DateTime), 1, 1)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (18, N'留言管理', N'/Blog/Feedback', N'icon-jibenziliao', 2, 11, 1, CAST(0x0000AA770103C6BC AS DateTime), CAST(0x0000AA7D00B8275C AS DateTime), 1, 1)
INSERT [dbo].[Guo_AdminMenu] ([ID], [MenuName], [MenuUrl], [MenuIcon], [OrderNo], [ParentId], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy]) VALUES (19, N'用户管理', N'/Blog/QQUser', N'icon-jibenziliao', 5, 11, 1, CAST(0x0000AA7701040604 AS DateTime), CAST(0x0000AA7C01477998 AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[Guo_AdminMenu] OFF
/****** Object:  Table [dbo].[Guo_Admin_Role_Menu]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Admin_Role_Menu](
	[RowId] [int] IDENTITY(1,1) NOT NULL,
	[MenuId] [int] NOT NULL,
	[RoleId] [int] NULL,
	[ActionId] [int] NULL,
 CONSTRAINT [PK_Guo_Admin_Role_Menu] PRIMARY KEY CLUSTERED 
(
	[RowId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Admin_Role_Menu] ON
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (13, 4, 0, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (32, 11, 0, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (34, 12, 0, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (40, 13, 0, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (46, 14, 0, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (52, 15, 0, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (58, 16, 0, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (64, 17, 0, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (68, 18, 0, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (72, 19, 0, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (143, 1, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (144, 2, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (145, 2, 1, 1)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (146, 2, 1, 2)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (147, 2, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (148, 2, 1, 8)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (149, 3, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (150, 3, 1, 1)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (151, 3, 1, 2)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (152, 3, 1, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (153, 3, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (154, 3, 1, 6)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (155, 4, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (156, 4, 1, 1)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (157, 4, 1, 2)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (158, 4, 1, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (159, 4, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (160, 4, 1, 5)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (161, 5, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (162, 5, 1, 1)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (163, 5, 1, 2)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (164, 5, 1, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (165, 5, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (166, 6, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (167, 7, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (168, 8, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (169, 9, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (170, 10, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (171, 10, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (172, 10, 1, 7)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (173, 11, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (174, 12, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (175, 12, 1, 1)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (176, 12, 1, 2)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (177, 12, 1, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (178, 12, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (179, 13, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (180, 13, 1, 1)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (181, 13, 1, 2)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (182, 13, 1, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (183, 13, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (184, 14, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (185, 14, 1, 1)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (186, 14, 1, 2)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (187, 14, 1, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (188, 14, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (189, 15, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (190, 15, 1, 1)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (191, 15, 1, 2)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (192, 15, 1, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (193, 15, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (194, 16, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (195, 16, 1, 1)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (196, 16, 1, 2)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (197, 16, 1, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (198, 16, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (199, 17, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (200, 17, 1, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (201, 17, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (202, 18, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (203, 18, 1, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (204, 18, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (205, 19, 1, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (206, 19, 1, 2)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (207, 19, 1, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (208, 19, 1, 4)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (223, 6, 2, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (224, 8, 2, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (225, 9, 2, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (226, 11, 2, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (227, 16, 2, 0)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (228, 16, 2, 1)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (229, 16, 2, 2)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (230, 16, 2, 3)
INSERT [dbo].[Guo_Admin_Role_Menu] ([RowId], [MenuId], [RoleId], [ActionId]) VALUES (231, 16, 2, 4)
SET IDENTITY_INSERT [dbo].[Guo_Admin_Role_Menu] OFF
/****** Object:  Table [dbo].[Guo_Action]    Script Date: 10/16/2020 11:40:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Action](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActionCode] [nvarchar](50) NULL,
	[ActionName] [nvarchar](50) NULL,
	[Position] [int] NULL,
	[Icon] [nvarchar](80) NULL,
	[Method] [nvarchar](50) NULL,
	[Remark] [nvarchar](80) NULL,
	[OrderBy] [int] NULL,
	[Status] [bit] NULL,
	[CreateOn] [datetime] NULL,
	[UpdateOn] [datetime] NULL,
	[CreateBy] [int] NULL,
	[UpdateBy] [int] NULL,
	[ClassName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Guo_Action] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Action] ON
INSERT [dbo].[Guo_Action] ([ID], [ActionCode], [ActionName], [Position], [Icon], [Method], [Remark], [OrderBy], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy], [ClassName]) VALUES (1, N'Add', N'添加', 1, N'icon-add', N'Add', N'', 0, 1, CAST(0x0000AA0200F2F2EC AS DateTime), CAST(0x0000AC370183D974 AS DateTime), 0, 1, N'')
INSERT [dbo].[Guo_Action] ([ID], [ActionCode], [ActionName], [Position], [Icon], [Method], [Remark], [OrderBy], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy], [ClassName]) VALUES (2, N'edit', N'编辑', 0, N'icon-bianji', N'edit', N'', 0, 1, CAST(0x0000AA0200F2F670 AS DateTime), CAST(0x0000AA0200F32B2C AS DateTime), 0, 0, N'')
INSERT [dbo].[Guo_Action] ([ID], [ActionCode], [ActionName], [Position], [Icon], [Method], [Remark], [OrderBy], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy], [ClassName]) VALUES (3, N'detail', N'查看', 0, N'icon-chakan', N'detail', N'', 0, 1, CAST(0x0000AA0200F2F9F4 AS DateTime), CAST(0x0000AA0200F32EB0 AS DateTime), 0, 0, N'layui-btn-normal')
INSERT [dbo].[Guo_Action] ([ID], [ActionCode], [ActionName], [Position], [Icon], [Method], [Remark], [OrderBy], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy], [ClassName]) VALUES (4, N'del', N'删除', 0, N'icon-guanbi', N'del', N'', 0, 1, CAST(0x0000AA0200F2FD78 AS DateTime), CAST(0x0000AA0200F33234 AS DateTime), 0, 0, N'layui-btn-danger')
INSERT [dbo].[Guo_Action] ([ID], [ActionCode], [ActionName], [Position], [Icon], [Method], [Remark], [OrderBy], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy], [ClassName]) VALUES (5, N'reset', N'重置密码', 0, N'icon-reset', N'reset', N'', 0, 1, CAST(0x0000AA0200F300FC AS DateTime), CAST(0x0000AA6D01849E54 AS DateTime), 0, 1, N'layui-btn-warm')
INSERT [dbo].[Guo_Action] ([ID], [ActionCode], [ActionName], [Position], [Icon], [Method], [Remark], [OrderBy], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy], [ClassName]) VALUES (6, N'assign', N'分配权限', 0, N'icon-jiaoseguanli', N'assign', N'', 0, 1, CAST(0x0000AA0200F30480 AS DateTime), CAST(0x0000AB6F00F33A68 AS DateTime), 0, 0, N'')
INSERT [dbo].[Guo_Action] ([ID], [ActionCode], [ActionName], [Position], [Icon], [Method], [Remark], [OrderBy], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy], [ClassName]) VALUES (7, N'BatchDel', N'批量删除', 1, N'icon-shanchu', N'BatchDel', N'', 0, 1, CAST(0x0000AA6D01846F74 AS DateTime), CAST(0x0000AB3500000000 AS DateTime), 1, 0, N'')
INSERT [dbo].[Guo_Action] ([ID], [ActionCode], [ActionName], [Position], [Icon], [Method], [Remark], [OrderBy], [Status], [CreateOn], [UpdateOn], [CreateBy], [UpdateBy], [ClassName]) VALUES (8, N'menu_action', N'菜单权限', 0, N'icon-setting-permissions', N'menu_action', N'', 0, 1, CAST(0x0000AA6F011848BC AS DateTime), CAST(0x0000AB3500000000 AS DateTime), 1, 0, N'')
SET IDENTITY_INSERT [dbo].[Guo_Action] OFF
/****** Object:  Table [dbo].[Guo_Acticle]    Script Date: 10/16/2020 11:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guo_Acticle](
	[ArticleID] [int] IDENTITY(1,1) NOT NULL,
	[TypeID] [int] NULL,
	[CatalogID] [int] NULL,
	[ArticleTitle] [nvarchar](50) NULL,
	[ImgUrl] [nvarchar](500) NULL,
	[Content] [ntext] NULL,
	[Abstract] [nvarchar](500) NULL,
	[CRT_Time] [datetime] NOT NULL,
	[Up_Time] [datetime] NULL,
	[UserID] [int] NULL,
	[ViewTimes] [int] NULL,
	[Replies] [int] NULL,
	[IsTop] [bit] NULL,
	[IsActive] [bit] NULL,
	[Wyyun] [nvarchar](500) NULL,
 CONSTRAINT [PK_Guo_Acticle] PRIMARY KEY CLUSTERED 
(
	[ArticleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Guo_Acticle] ON
INSERT [dbo].[Guo_Acticle] ([ArticleID], [TypeID], [CatalogID], [ArticleTitle], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive], [Wyyun]) VALUES (1, 1, 1, N'Java学习路线推荐11', N'/Upload/img/1.jpg', N'<h6>正文</h6><p>这篇文章是为了介绍自己自学用过的Java视频资料。本套整合教程总共180+G，共450+小时。考虑到绝大部分视频至少要看两遍，而且视频总时长并不代表学习时长，所以零基础初学者总学习时间大约为：600小时视频时长 + 100小时理解 + 100小时练习，至少需要800小时。你可能觉得自己能一天学习8小时，实际上平均下来每天能学4小时都算厉害了。总会有各种原因，比如当天内容太难，公司聚会，要出差等等。如果周末你也是坚持学习，那么最理想状况下，6个半月就可以学完，达到工作后能被人带的水平。但我知道那其实基本不可能。</p>', N'这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要', CAST(0x0000AC2A00000000 AS DateTime), CAST(0x0000AC4200B278E0 AS DateTime), 1, 119, 5, 1, 1, NULL)
INSERT [dbo].[Guo_Acticle] ([ArticleID], [TypeID], [CatalogID], [ArticleTitle], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive], [Wyyun]) VALUES (2, 1, 2, N'C#学习路线推荐', N'/Upload/img/2.jpg', N'<h6>正文</h6><p>这篇文章是为了介绍自己自学用过的Java视频资料。本套整合教程总共180+G，共450+小时。考虑到绝大部分视频至少要看两遍，而且视频总时长并不代表学习时长，所以零基础初学者总学习时间大约为：600小时视频时长 + 100小时理解 + 100小时练习，至少需要800小时。你可能觉得自己能一天学习8小时，实际上平均下来每天能学4小时都算厉害了。总会有各种原因，比如当天内容太难，公司聚会，要出差等等。如果周末你也是坚持学习，那么最理想状况下，6个半月就可以学完，达到工作后能被人带的水平。但我知道那其实基本不可能。</p>', N'1这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要', CAST(0x0000AC2A00000000 AS DateTime), CAST(0x0000AC4200C1ECDA AS DateTime), 1, 54, 1, 1, 1, NULL)
INSERT [dbo].[Guo_Acticle] ([ArticleID], [TypeID], [CatalogID], [ArticleTitle], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive], [Wyyun]) VALUES (4, 2, 2, N'Java学习路线推荐', N'/Upload/img/1.jpg', N'这篇文章是为了介绍自己自学用过的Java视频资料。本套整合教程总共180+G，共450+小时。考虑到绝大部分视频至少要看两遍，而且视频总时长并不代表学习时长，所以零基础初学者总学习时间大约为：600小时视频时长 + 100小时理解 + 100小时练习，至少需要800小时。你可能觉得自己能一天学习8小时，实际上平均下来每天能学4小时都算厉害了。总会有各种原因，比如当天内容太难，公司聚会，要出差等等。如果周末你也是坚持学习，那么最理想状况下，6个半月就可以学完，达到工作后能被人带的水平。但我知道那其实基本不可能。', N'3这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要', CAST(0x0000AC2A00000000 AS DateTime), NULL, 1, 101, 5, 0, 1, NULL)
INSERT [dbo].[Guo_Acticle] ([ArticleID], [TypeID], [CatalogID], [ArticleTitle], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive], [Wyyun]) VALUES (6, 1, 1, N'Java学习路线推荐', N'/Upload/img/3.jpg', N'这篇文章是为了介绍自己自学用过的Java视频资料。本套整合教程总共180+G，共450+小时。考虑到绝大部分视频至少要看两遍，而且视频总时长并不代表学习时长，所以零基础初学者总学习时间大约为：600小时视频时长 + 100小时理解 + 100小时练习，至少需要800小时。你可能觉得自己能一天学习8小时，实际上平均下来每天能学4小时都算厉害了。总会有各种原因，比如当天内容太难，公司聚会，要出差等等。如果周末你也是坚持学习，那么最理想状况下，6个半月就可以学完，达到工作后能被人带的水平。但我知道那其实基本不可能。', N'5这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要', CAST(0x0000AC2A00000000 AS DateTime), NULL, 1, 102, 5, 0, 1, NULL)
INSERT [dbo].[Guo_Acticle] ([ArticleID], [TypeID], [CatalogID], [ArticleTitle], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive], [Wyyun]) VALUES (7, 1, 1, N'Vue学习路线推荐', N'/Upload/img/1.jpg', N'这篇文章是为了介绍自己自学用过的Java视频资料。本套整合教程总共180+G，共450+小时。考虑到绝大部分视频至少要看两遍，而且视频总时长并不代表学习时长，所以零基础初学者总学习时间大约为：600小时视频时长 + 100小时理解 + 100小时练习，至少需要800小时。你可能觉得自己能一天学习8小时，实际上平均下来每天能学4小时都算厉害了。总会有各种原因，比如当天内容太难，公司聚会，要出差等等。如果周末你也是坚持学习，那么最理想状况下，6个半月就可以学完，达到工作后能被人带的水平。但我知道那其实基本不可能。', N'6这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要', CAST(0x0000AC2A00000000 AS DateTime), NULL, 1, 105, 5, 0, 1, NULL)
INSERT [dbo].[Guo_Acticle] ([ArticleID], [TypeID], [CatalogID], [ArticleTitle], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive], [Wyyun]) VALUES (8, 2, 2, N'C语言学习路线推荐', N'/Upload/img/1.jpg', N'这篇文章是为了介绍自己自学用过的Java视频资料。本套整合教程总共180+G，共450+小时。考虑到绝大部分视频至少要看两遍，而且视频总时长并不代表学习时长，所以零基础初学者总学习时间大约为：600小时视频时长 + 100小时理解 + 100小时练习，至少需要800小时。你可能觉得自己能一天学习8小时，实际上平均下来每天能学4小时都算厉害了。总会有各种原因，比如当天内容太难，公司聚会，要出差等等。如果周末你也是坚持学习，那么最理想状况下，6个半月就可以学完，达到工作后能被人带的水平。但我知道那其实基本不可能。', N'3这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要', CAST(0x0000AC2A00000000 AS DateTime), NULL, 1, 100, 5, 0, 1, NULL)
INSERT [dbo].[Guo_Acticle] ([ArticleID], [TypeID], [CatalogID], [ArticleTitle], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive], [Wyyun]) VALUES (9, 2, 1, N'Go语言学习路线推荐', N'/Upload/img/2.jpg', N'这篇文章是为了介绍自己自学用过的Java视频资料。本套整合教程总共180+G，共450+小时。考虑到绝大部分视频至少要看两遍，而且视频总时长并不代表学习时长，所以零基础初学者总学习时间大约为：600小时视频时长 + 100小时理解 + 100小时练习，至少需要800小时。你可能觉得自己能一天学习8小时，实际上平均下来每天能学4小时都算厉害了。总会有各种原因，比如当天内容太难，公司聚会，要出差等等。如果周末你也是坚持学习，那么最理想状况下，6个半月就可以学完，达到工作后能被人带的水平。但我知道那其实基本不可能。', N'4这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要', CAST(0x0000AC2A00000000 AS DateTime), NULL, 1, 106, 5, 1, 1, NULL)
INSERT [dbo].[Guo_Acticle] ([ArticleID], [TypeID], [CatalogID], [ArticleTitle], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive], [Wyyun]) VALUES (10, 1, 1, N'F#学习路线推荐', N'/Upload/img/3.jpg', N'这篇文章是为了介绍自己自学用过的Java视频资料。本套整合教程总共180+G，共450+小时。考虑到绝大部分视频至少要看两遍，而且视频总时长并不代表学习时长，所以零基础初学者总学习时间大约为：600小时视频时长 + 100小时理解 + 100小时练习，至少需要800小时。你可能觉得自己能一天学习8小时，实际上平均下来每天能学4小时都算厉害了。总会有各种原因，比如当天内容太难，公司聚会，要出差等等。如果周末你也是坚持学习，那么最理想状况下，6个半月就可以学完，达到工作后能被人带的水平。但我知道那其实基本不可能。', N'5这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要这是摘要', CAST(0x0000AC2A00000000 AS DateTime), NULL, 1, 102, 5, 0, 1, NULL)
INSERT [dbo].[Guo_Acticle] ([ArticleID], [TypeID], [CatalogID], [ArticleTitle], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive], [Wyyun]) VALUES (13, 1, 4, N'无敌的我又回来了', N'/Upload/img/1_1.jpg', N'哈哈哈哈', N'今天主要完善了博客的 日记模块的前后端功能', CAST(0x0000AC32015D5B28 AS DateTime), CAST(0x0000AC4300C06A0D AS DateTime), 1, 3, NULL, 0, 1, NULL)
INSERT [dbo].[Guo_Acticle] ([ArticleID], [TypeID], [CatalogID], [ArticleTitle], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive], [Wyyun]) VALUES (17, 1, 4, N'我的个人博客终于到这里就快结束了', N'/Upload/img/bg9.jpg', N'<h6>正文</h6><p>历时23天，修修改改，也终于迎来了结束的一天，至少是暂时性的结束。很遗憾，即便认真做了，但终究是没有做出一个好的作品来，不过，就这样吧。</p><p><br></p><p></p><pre><code>var context = layedit.getContent(index);//这里是前面获取到index

                        if (context.length &gt; 0) {

                            if (context.indexOf(''&lt;'') &gt;= 0) {//需要进行解码

                                $(document.getElementById("LAY_layedit_" + index)).contents().find("body")

                                    .html(HtmlUtil.htmlDecode(context));

                            }

                            //同步到编辑框

                            layedit.sync(index);</code></pre><br><p></p>', N'这是一个摘要', CAST(0x0000AC460120BF88 AS DateTime), CAST(0x0000AC44011585A7 AS DateTime), 1, 52, NULL, 1, 1, N'https://music.163.com/outchain/player?type=2&amp;id=1363948882&amp;auto=0&amp;height=66')
INSERT [dbo].[Guo_Acticle] ([ArticleID], [TypeID], [CatalogID], [ArticleTitle], [ImgUrl], [Content], [Abstract], [CRT_Time], [Up_Time], [UserID], [ViewTimes], [Replies], [IsTop], [IsActive], [Wyyun]) VALUES (19, 1, 4, N'项目开源拉', N'/Upload/img/nature.jpg', N'<h6>正文</h6><p>本项目开源，您可以对此进行转载、二次创作等。只需您<span>在使用时注明出处。</span>但是您不得对其用于商业目的。只为提供给小白或者在学习的人使用。</p><p><span>毕竟也是写了这么一个月，尽管还是不满意，但是总算大体功能完成了。希望能给到人帮助吧。</span></p><p>项目地址：<a href="https://gitee.com/guo_zi_shan/yun-q_-blogs">https://gitee.com/guo_zi_shan/yun-q_-blogs</a></p><p>项目演示地址：<a href="http://www.guoqingyun.top/">http://www.guoqingyun.top/</a></p><p>欢迎大家来留言，学习哟<img src="http://localhost:13174/Areas/Admin/plugins/layui/images/face/1.gif" alt="[嘻嘻]"></p>', N'此项目为个人业余作品，也参考了一些大佬的作品。为了给刚入门的小白学习，特此开源。', CAST(0x0000AC5500AFB540 AS DateTime), CAST(0x0000AC5600B6B334 AS DateTime), 1, 3, NULL, 1, 1, N'https://music.163.com/outchain/player?type=2&amp;id=1363948882&amp;auto=0&amp;height=66')
SET IDENTITY_INSERT [dbo].[Guo_Acticle] OFF
/****** Object:  UserDefinedFunction [dbo].[CIntToHex]    Script Date: 10/16/2020 11:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  FUNCTION [dbo].[CIntToHex](@IntValue BIGINT)
RETURNS VARCHAR(16)
--@Encrypt$--
AS
BEGIN
	DECLARE @ReChar VARCHAR(16)
	DECLARE @HexString VARCHAR(16)
	DECLARE @HeadInt BIGINT, @ModInt TinyInt
	DECLARE @C CHAR(1)

	SET @HexString = '0123456789ABCDEF'

	SET @ReChar = ISNULL(@ReChar, '')

	SET @HeadInt = FLOOR(@IntValue / 16)
	SET @ModInt = @IntValue - @HeadInt * 16

	SET @C = SUBSTRING(@HexString, @ModInt + 1, 1)

	IF @HeadInt > 0
		SET @ReChar = dbo.CIntToHex(@HeadInt) + @C
	ELSE
		SET @ReChar = @C

	RETURN @ReChar	
END
GO
/****** Object:  StoredProcedure [dbo].[UP_Reply_AddNew]    Script Date: 10/16/2020 11:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--UP_Reply_AddNew 1,55,'hahahah','61','1'
Create    PROCEDURE [dbo].[UP_Reply_AddNew](
	@ArticleID	INT,	--栏目ID
	@ReplyerID	INT,
	@ReceiverIDs NVARCHAR(4000),
	@Content NVARCHAR(4000),
	@IsActive TINYINT
)
AS
DECLARE @tempActive INT
SET @tempActive = @IsActive
IF @IsActive = NULL OR @IsActive < 0
	SET @tempActive = 1
INSERT INTO Guo_CatalogReplys(ArticleID, ReplyerID, ReceiverIDs, Content, IsActive) 
VALUES(@ArticleID, @ReplyerID, @ReceiverIDs, @Content, @tempActive )
--PRINT @SQL
--PRINT @@ERROR
IF @@ERROR = 0
RETURN 1
ELSE
RETURN -1
GO
/****** Object:  UserDefinedFunction [dbo].[BRPEncrypt]    Script Date: 10/16/2020 11:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** 对象:  用户定义的函数 dbo.BRPEncrypt    脚本日期: 2020-8-21 08:56:39 ******/

CREATE     FUNCTION [dbo].[BRPEncrypt](@strSource VARCHAR(1600), @EncPara INT) 
RETURNS VARCHAR(8000)
AS
BEGIN
	DECLARE @Re VARCHAR(8000)
	SET @Re = ''

	SET @EncPara = @EncPara % 256

	SELECT @Re = @Re +  
		CASE	WHEN UNICODE(value) > 1024 
			THEN 'G' +  dbo.CIntToHex(UNICODE(value) ^ (10000 + (@EncPara * RowID) % 256))
			ELSE 
				CASE	WHEN LEN(dbo.CIntToHex(UNICODE(value) ^ ((@EncPara * RowID) % 256))) = 1
					THEN  '0' + dbo.CIntToHex(UNICODE(value) ^ ((@EncPara * RowID) % 256))
					ELSE
						dbo.CIntToHex(UNICODE(value) ^ ((@EncPara * RowID) % 256))
					END
		END
	FROM dbo.StrToChar(@strSource)

	RETURN @Re
END
GO
/****** Object:  Default [DF_Guo_Sys_Menu_REL]    Script Date: 10/16/2020 11:40:51 ******/
ALTER TABLE [dbo].[Guo_Sys_Menu] ADD  CONSTRAINT [DF_Guo_Sys_Menu_REL]  DEFAULT ((0)) FOR [REL]
GO
/****** Object:  Default [DF_Guo_Sys_Menu_Caption]    Script Date: 10/16/2020 11:40:51 ******/
ALTER TABLE [dbo].[Guo_Sys_Menu] ADD  CONSTRAINT [DF_Guo_Sys_Menu_Caption]  DEFAULT ('') FOR [Caption]
GO
/****** Object:  Default [DF_Guo_Sys_Menu_URL]    Script Date: 10/16/2020 11:40:51 ******/
ALTER TABLE [dbo].[Guo_Sys_Menu] ADD  CONSTRAINT [DF_Guo_Sys_Menu_URL]  DEFAULT ('') FOR [URL]
GO
/****** Object:  Default [DF_Guo_Sys_Menu_IsHid]    Script Date: 10/16/2020 11:40:51 ******/
ALTER TABLE [dbo].[Guo_Sys_Menu] ADD  CONSTRAINT [DF_Guo_Sys_Menu_IsHid]  DEFAULT ((0)) FOR [IsHid]
GO
/****** Object:  Default [DF_Guo_Acticle_ViewTimes]    Script Date: 10/16/2020 11:40:52 ******/
ALTER TABLE [dbo].[Guo_Acticle] ADD  CONSTRAINT [DF_Guo_Acticle_ViewTimes]  DEFAULT ((0)) FOR [ViewTimes]
GO
