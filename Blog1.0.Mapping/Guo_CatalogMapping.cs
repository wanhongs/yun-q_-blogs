﻿using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Mapping
{
    public class Guo_CatalogMapping : EntityTypeConfiguration<Guo_Catalog>
    {
        public Guo_CatalogMapping()
        {
            this.ToTable("Guo_Catalog");
            this.HasKey(t => t.CatalogID);
        }
    }
}
