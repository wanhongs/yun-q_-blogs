﻿using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Mapping
{
    public class Guo_Menu_ActionMapping : EntityTypeConfiguration<Guo_Menu_Action>
    {
        public Guo_Menu_ActionMapping()
        {
            this.ToTable("Guo_Menu_Action");
            this.HasKey(t => t.MenuId);
        }
    }
}
