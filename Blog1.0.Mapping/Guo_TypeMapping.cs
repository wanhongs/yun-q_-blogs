﻿using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Mapping
{
    public class Guo_TypeMapping : EntityTypeConfiguration<Guo_Type>
    {
        public Guo_TypeMapping()
        {
            this.ToTable("Guo_Type");
            this.HasKey(t => t.TypeID);
        }
    }
}
