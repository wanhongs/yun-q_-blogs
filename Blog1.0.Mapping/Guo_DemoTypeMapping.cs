﻿using Blog1._0.Model;
using System.Data.Entity.ModelConfiguration;

namespace Blog1._0.Mapping
{
    public class Guo_DemoTypeMapping : EntityTypeConfiguration<Guo_DemoType>
    {
        public Guo_DemoTypeMapping()
        {
            this.ToTable("Guo_DemoType");
            this.HasKey(t => t.Id);
        }
    }
}
